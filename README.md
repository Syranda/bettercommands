# BetterCommands

Note: This is just a small introduction. For an extensive tutorial check the [wiki](https://gitlab.com/Syranda/bettercommands/wikis/home).

BetterCommands is a annotation driven command API to make your commands cleaner, more structured and easier to control.<br/>
The key components the API offers are:
+ Easy setup
+ Strutured command paramters
+ Inline paramters (e.g. /pex \<player\> add \<permission\> | \<player\> = inline parameter)
+ Automatic parameter validation
+ Automatic help sub command for each registired command (e.g. '/\<command\> help')
+ Beautiful color management
+ Predefined permissions

# Download
Due to dependency issues the jar cannot be build via CI runnter.

For available versions and downloads click [here](http://dev.cantoros-server.de/artifactory/webapp/#/artifacts/browse/tree/General/public/de/syranda/bettercommands)

# Setup
Add this to your pom:<br/>
```xml
<repositories>
    ...	
	<repository>
		<id>cantoros-server</id>
		<url>http://dev.cantoros-server.de/artifactory/public</url>
	</repository>
    ...
</repositories>
<dependencies>
	...
	<dependency>
		<groupId>de.syranda</groupId>
		<artifactId>bettercommands</artifactId>
		<version>2.0.0-BETA-13</version>
	</dependency>
    ...
</dependencies>
```
Don't forget to add the dependency to your plugin.yml
```yml
depend: [BetterCommands, ...]
```
To make the API scan your projects use this in your 'onEnable' part:
```java
@Override
public void onEnable() {
    // ...
	BetterCommand.scan(this);
	// ...
}
```
The API now reigsters all commands and other classes which are meant to be used by the API (e.g. custom parameters, color configs)
# Example
An easy command could look like this:
```java
@Command // -> Identifier, when not specified the method name will be the command's name ('/example')
public void example(CommandSender sender) {
	sender.sendMessage("I'm working");
}
```