package de.syranda.bettercommands;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.Permissible;
import org.bukkit.plugin.SimplePluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import de.syranda.bettercommands.customclasses.BetterCommand;
import de.syranda.bettercommands.customclasses.config.BetterCommandConfig;
import de.syranda.bettercommands.customclasses.nativeoverride.BetterCommandsConsoleCommandSender;
import de.syranda.bettercommands.customclasses.permissionhooks.HookRegistry;
import de.syranda.bettercommands.listener.ServerCommandListener;
import net.md_5.bungee.api.ChatColor;

public class Main extends JavaPlugin {
		
	private static Main instance;
	private static BetterCommandsConsoleCommandSender commandSender;
	
	public static Main getInstance() {
		return instance;
	}	
	
	public static BetterCommandsConsoleCommandSender getCommandSender() {
		return commandSender;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void onEnable() {	
		
		instance = this;	
		commandSender = new BetterCommandsConsoleCommandSender();
		
		new ServerCommandListener(this);
	
		ConfigValues.loadConfig();
		BetterCommand.excludePackage("de.syranda.bettercommands.customclasses.config.Default");
		BetterCommand.scan(this);
		
		Bukkit.getScheduler().runTaskLater(this, () -> {
			try {
				
				Field field = SimplePluginManager.class.getDeclaredField("permSubs");
				field.setAccessible(true);
				
				Map<String, Map<Permissible, Boolean>> map = (Map<String, Map<Permissible, Boolean>>) field.get(Bukkit.getPluginManager());			
				Map<Permissible, Boolean> newConsole = new HashMap<>();
				newConsole.put(commandSender, true);
				map.put("bukkit.broadcast.user", newConsole);				
				field.set(Bukkit.getPluginManager(), map);
				
			} catch(Exception e) {
				e.printStackTrace();
			}
			HookRegistry.setUsedHook(HookRegistry.getHooks().size() == 1 ? HookRegistry.getHooks().get(0).getSimpleName() : ConfigValues.PERMISSION_SYSTEM);
			System.out.println("[BetterCommands] Using permission hook '" + HookRegistry.getUsedHook() + "'");
			
		}, 5);
		
	}

	public static void sendExampleContent(CommandSender sender, BetterCommandConfig config) {
		
		BetterCommand.disableTranslator(sender);
		
		sender.sendMessage(ChatColor.GRAY + "============== " + ChatColor.GOLD + "Colors "+ ChatColor.GRAY + "==============");		
		sender.sendMessage(config.getNormalColor() + "This is the normal color");
		sender.sendMessage(config.getTranslator().translate(config, ":nThis is the normal color (Translator)").toLegacyText());
		sender.sendMessage(config.getTranslator().translateError(config, ":nThis is the normal color (Error translator)").toLegacyText());
		sender.sendMessage(config.getSubNormalColor() + "This is the sub normal color");
		sender.sendMessage(config.getTranslator().translate(config, ":snThis is the sub normal color (Translator)").toLegacyText());
		sender.sendMessage(config.getTranslator().translateError(config, ":snThis is the sub normal color (Error translator)").toLegacyText());
		sender.sendMessage(config.getHighlightColor() + "This is the highlight color");
		sender.sendMessage(config.getTranslator().translate(config, ":hThis is the highlight color (Translator)").toLegacyText());
		sender.sendMessage(config.getTranslator().translateError(config, ":hThis is the highlight color (Error translator)").toLegacyText());
		sender.sendMessage(config.getSubHighlightColor() + "This is the sub highlight color");
		sender.sendMessage(config.getTranslator().translate(config, ":shThis is the sub highlight color (Translator)").toLegacyText());
		sender.sendMessage(config.getTranslator().translateError(config, ":shThis is the sub highlight color (Error translator)").toLegacyText());
		sender.sendMessage(config.getSuccessColor() + "This is the success color");
		sender.sendMessage(config.getTranslator().translate(config, ":sThis is the success color (Translator)").toLegacyText());
		sender.sendMessage(config.getTranslator().translateError(config, ":sThis is the success color (Error translator)").toLegacyText());
		sender.sendMessage(config.getFailedColor() + "This is the error color");
		sender.sendMessage(config.getTranslator().translate(config, ":fThis is the error color (Translator)").toLegacyText());
		sender.sendMessage(config.getTranslator().translateError(config, ":fThis is the error color (Error translator)").toLegacyText());
		sender.sendMessage(config.getMinorColor() + "This is the minor content color");
		sender.sendMessage(config.getTranslator().translate(config, ":mThis is the minor content color (Translator)").toLegacyText());
		sender.sendMessage(config.getTranslator().translateError(config, ":mThis is the minor content color (Error translator)").toLegacyText());
		sender.sendMessage(ChatColor.GRAY + "============== " + ChatColor.GOLD + "Translators "+ ChatColor.GRAY + "==============");	
		sender.sendMessage(config.getTranslator().translate(config, "Plain text (Translator)").toLegacyText());
		sender.sendMessage(config.getTranslator().translateError(config, "Plain text (Errror translator)").toLegacyText());
		
		BetterCommand.restoreTranslator(sender);
		
	}
	
}
