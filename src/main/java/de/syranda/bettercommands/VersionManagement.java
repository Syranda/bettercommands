package de.syranda.bettercommands;

import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import de.syranda.bettercommands.customclasses.BetterCommand;
import de.syranda.bettercommands.customclasses.SubCommand;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.MessageToMessageEncoder;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;

public class VersionManagement {
	
	public static ChannelPipeline getCompatiblePipeline(Player player) {
		if(Bukkit.getBukkitVersion().startsWith("1.8"))			
			return (ChannelPipeline)((org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer) player).getHandle().playerConnection.networkManager.channel.pipeline();
		else if(Bukkit.getBukkitVersion().startsWith("1.9"))
			return (ChannelPipeline)((org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer) player).getHandle().playerConnection.networkManager.channel.pipeline();	
		else if(Bukkit.getBukkitVersion().startsWith("1.10"))
			return (ChannelPipeline)((org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer) player).getHandle().playerConnection.networkManager.channel.pipeline();	
		else if(Bukkit.getBukkitVersion().startsWith("1.11"))
			return (ChannelPipeline)((org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer) player).getHandle().playerConnection.networkManager.channel.pipeline();
		else if(Bukkit.getBukkitVersion().startsWith("1.12"))
			return (ChannelPipeline)((org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer) player).getHandle().playerConnection.networkManager.channel.pipeline();
		else
			throw new RuntimeException("No compatible pipeline was found");
	}

	public static void registerCommand(BetterCommand betterCommand) {
		if(Bukkit.getBukkitVersion().startsWith("1.8"))			
			((org.bukkit.craftbukkit.v1_8_R3.CraftServer) Bukkit.getServer()).getCommandMap().register(betterCommand.getCommand(), betterCommand);	
		else if(Bukkit.getBukkitVersion().startsWith("1.9"))
			((org.bukkit.craftbukkit.v1_9_R2.CraftServer) Bukkit.getServer()).getCommandMap().register(betterCommand.getCommand(), betterCommand);	
		else if(Bukkit.getBukkitVersion().startsWith("1.10"))
			((org.bukkit.craftbukkit.v1_10_R1.CraftServer) Bukkit.getServer()).getCommandMap().register(betterCommand.getCommand(), betterCommand);	
		else if(Bukkit.getBukkitVersion().startsWith("1.11"))
			((org.bukkit.craftbukkit.v1_11_R1.CraftServer) Bukkit.getServer()).getCommandMap().register(betterCommand.getCommand(), betterCommand);	
		else if(Bukkit.getBukkitVersion().startsWith("1.12"))
			((org.bukkit.craftbukkit.v1_12_R1.CraftServer) Bukkit.getServer()).getCommandMap().register(betterCommand.getCommand(), betterCommand);
		else
			throw new RuntimeException("Incompatible version");		
	}
	
	@SuppressWarnings("rawtypes")
	public static MessageToMessageEncoder getCompatibleMessageEncoder(SubCommand subCommand) {
		
		if(Bukkit.getBukkitVersion().startsWith("1.8"))			
			return new MessageToMessageEncoder<net.minecraft.server.v1_8_R3.Packet<?>>() {
				protected void encode(ChannelHandlerContext ctx, net.minecraft.server.v1_8_R3.Packet<?> packet, List<Object> objs) throws Exception {
					
					if(packet.getClass().getName().equals(net.minecraft.server.v1_8_R3.PacketPlayOutChat.class.getName()) && get("a", packet) != null) {
						
						String text = ((net.minecraft.server.v1_8_R3.IChatBaseComponent) get("a", packet)).c();						
						TextComponent component = subCommand.getTranslator().translate(!text.startsWith("|"), subCommand, text.startsWith("|") ? text.substring(1) : text);
						set("components", new BaseComponent[] { component }, packet);
						
					}
					
					objs.add(packet);
	
				};
			};
		
		else if(Bukkit.getBukkitVersion().startsWith("1.9"))
			return new MessageToMessageEncoder<net.minecraft.server.v1_9_R2.Packet<?>>() {
				protected void encode(ChannelHandlerContext ctx, net.minecraft.server.v1_9_R2.Packet<?> packet, List<Object> objs) throws Exception {
					
					if(packet.getClass().getName().equals(net.minecraft.server.v1_9_R2.PacketPlayOutChat.class.getName()) && get("a", packet) != null) {
						
						String text = ((net.minecraft.server.v1_9_R2.IChatBaseComponent) get("a", packet)).toPlainText();
	
						TextComponent component = subCommand.getTranslator().translate(!text.startsWith("|"), subCommand, text.startsWith("|") ? text.substring(1) : text);
						set("components", new BaseComponent[] { component }, packet);
	
					}
	
					objs.add(packet);
	
				};
			};	
		else if(Bukkit.getBukkitVersion().startsWith("1.10"))
			return new MessageToMessageEncoder<net.minecraft.server.v1_10_R1.Packet<?>>() {
			protected void encode(ChannelHandlerContext ctx, net.minecraft.server.v1_10_R1.Packet<?> packet, List<Object> objs) throws Exception {
				
				if(packet.getClass().getName().equals(net.minecraft.server.v1_10_R1.PacketPlayOutChat.class.getName()) && get("a", packet) != null) {

					String text = loadWithChildren(((net.minecraft.server.v1_10_R1.IChatBaseComponent) get("a", packet)));
					
					TextComponent component = subCommand.getTranslator().translate(!text.startsWith("|"), subCommand, text.startsWith("|") ? text.substring(1) : text);
					set("components", new BaseComponent[] { component }, packet);

				}

				objs.add(packet);

			};
			private String loadWithChildren(net.minecraft.server.v1_10_R1.IChatBaseComponent component) {
				return toColorString(component) + component.a().stream().map(comp -> toColorString(comp)).collect(Collectors.joining());
			}
			private String toColorString(net.minecraft.server.v1_10_R1.IChatBaseComponent component) {
				
				StringBuilder ret = new StringBuilder();
				net.minecraft.server.v1_10_R1.ChatModifier modifier = component.getChatModifier();				
				
				if(component.getChatModifier().getColor() != null) {
					ChatColor color = ChatColor.valueOf(component.getChatModifier().getColor().name());
					if(color != ChatColor.RESET)
						ret.append(ChatColor.valueOf(component.getChatModifier().getColor().name()));
				} if(modifier.isBold())
					ret.append(ChatColor.BOLD);
				if(modifier.isItalic())
					ret.append(ChatColor.ITALIC);
				if(modifier.isStrikethrough())
					ret.append(ChatColor.STRIKETHROUGH);
				if(modifier.isRandom())
					ret.append(ChatColor.MAGIC);
				
				ret.append(component.getText());
				
				return ret.toString();
					
			}
		};
		else if(Bukkit.getBukkitVersion().startsWith("1.11"))
			return new MessageToMessageEncoder<net.minecraft.server.v1_11_R1.Packet<?>>() {
			protected void encode(ChannelHandlerContext ctx, net.minecraft.server.v1_11_R1.Packet<?> packet, List<Object> objs) throws Exception {
				
				if(packet.getClass().getName().equals(net.minecraft.server.v1_11_R1.PacketPlayOutChat.class.getName()) && get("a", packet) != null) {
					
					String text = loadWithChildren(((net.minecraft.server.v1_11_R1.IChatBaseComponent) get("a", packet)));

					TextComponent component = subCommand.getTranslator().translate(!text.startsWith("|"), subCommand, text.startsWith("|") ? text.substring(1) : text);
					set("components", new BaseComponent[] { component }, packet);

				}

				objs.add(packet);

			};
			private String loadWithChildren(net.minecraft.server.v1_11_R1.IChatBaseComponent component) {
				return toColorString(component) + component.a().stream().map(comp -> toColorString(comp)).collect(Collectors.joining());
			}
			private String toColorString(net.minecraft.server.v1_11_R1.IChatBaseComponent component) {
				
				StringBuilder ret = new StringBuilder();
				net.minecraft.server.v1_11_R1.ChatModifier modifier = component.getChatModifier();				
				
				if(component.getChatModifier().getColor() != null) {
					ChatColor color = ChatColor.valueOf(component.getChatModifier().getColor().name());
					if(color != ChatColor.RESET)
						ret.append(ChatColor.valueOf(component.getChatModifier().getColor().name()));
				} if(modifier.isBold())
					ret.append(ChatColor.BOLD);
				if(modifier.isItalic())
					ret.append(ChatColor.ITALIC);
				if(modifier.isStrikethrough())
					ret.append(ChatColor.STRIKETHROUGH);
				if(modifier.isRandom())
					ret.append(ChatColor.MAGIC);
				
				ret.append(component.getText());
				
				return ret.toString();
					
			}
		};
		else if(Bukkit.getBukkitVersion().startsWith("1.12"))
			return new MessageToMessageEncoder<net.minecraft.server.v1_12_R1.Packet<?>>() {
			protected void encode(ChannelHandlerContext ctx, net.minecraft.server.v1_12_R1.Packet<?> packet, List<Object> objs) throws Exception {
				
				if(packet.getClass().getName().equals(net.minecraft.server.v1_12_R1.PacketPlayOutChat.class.getName()) && get("a", packet) != null) {
					
					String text = loadWithChildren(((net.minecraft.server.v1_12_R1.IChatBaseComponent) get("a", packet)));
					TextComponent component = subCommand.getTranslator().translate(!text.startsWith("|"), subCommand, text.startsWith("|") ? text.substring(1) : text);
					set("components", new BaseComponent[] { component }, packet);

				}

				objs.add(packet);

			};
			private String loadWithChildren(net.minecraft.server.v1_12_R1.IChatBaseComponent component) {
				return toColorString(component) + component.a().stream().map(comp -> toColorString(comp)).collect(Collectors.joining());
			}
			private String toColorString(net.minecraft.server.v1_12_R1.IChatBaseComponent component) {
				StringBuilder ret = new StringBuilder();
				net.minecraft.server.v1_12_R1.ChatModifier modifier = component.getChatModifier();				
				
				if(component.getChatModifier().getColor() != null) {
					ChatColor color = ChatColor.valueOf(component.getChatModifier().getColor().name());
					if(color != ChatColor.RESET)
						ret.append(ChatColor.valueOf(component.getChatModifier().getColor().name()));
				} if(modifier.isBold())
					ret.append(ChatColor.BOLD);
				if(modifier.isItalic())
					ret.append(ChatColor.ITALIC);
				if(modifier.isStrikethrough())
					ret.append(ChatColor.STRIKETHROUGH);
				if(modifier.isRandom())
					ret.append(ChatColor.MAGIC);
				
				ret.append(component.getText());
				
				return ret.toString();
					
			}
		};
		else
			throw new RuntimeException("No compatible message encoder found");	
		
	}
	
	private static Object get(String field, Object from) {

		try {
			Field f = from.getClass().getDeclaredField(field);
			f.setAccessible(true);
			return f.get(from);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException
				| SecurityException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static void set(String field, Object to, Object from) {
		try {
			Field f = from.getClass().getDeclaredField(field);
			f.setAccessible(true);
			f.set(from, to);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException
				| SecurityException e) {
			e.printStackTrace();
		}
	}
	
}
