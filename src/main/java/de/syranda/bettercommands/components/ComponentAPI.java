package de.syranda.bettercommands.components;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class ComponentAPI {

	public static BaseComponent getComponent(String text, String hoverText) {
		return getComponent(text, hoverText, null, null);
	}
	
	public static BaseComponent getComponent(String text, ClickEvent.Action action, String value) {
		return getComponent(text, null, action, value);	
	}
	
	public static BaseComponent getComponent(String text, String hoverText, ClickEvent.Action action, String value) {
		
		TextComponent textComponent = new TextComponent(text);
		
		if(hoverText != null)
			textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(hoverText).create()));
		
		if(action != null && value != null)
			textComponent.setClickEvent(new ClickEvent(action, ChatColor.stripColor(value)));
		
		return textComponent;
		
	}
	
}
