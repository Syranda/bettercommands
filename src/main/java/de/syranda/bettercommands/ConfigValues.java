package de.syranda.bettercommands;

import de.syranda.bettercommands.customclasses.CommandValue;

public class ConfigValues {
	
	public static char COLOR_CHAR = ':';
	public static String CONVERSATION_ESCAPE_SEQUENCE = "/exit";
	public static String OPTIONAL_NONE = "null";
	
	public static String PERMISSION_SYSTEM;
	
	public static void loadConfig() {
		
		PERMISSION_SYSTEM = set("Permission.Hook", "PermissionsEx").asString();
		Main.getInstance().getConfig().options().copyDefaults(true);
		Main.getInstance().saveConfig();
		
	}
	
	@SuppressWarnings("rawtypes")
	private static CommandValue<?> set(String path, Object value) {
		Main.getInstance().getConfig().addDefault(path, value);
		return new CommandValue(Main.getInstance().getConfig().get(path));
	}
}
