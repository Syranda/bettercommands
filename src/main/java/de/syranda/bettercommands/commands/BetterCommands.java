package de.syranda.bettercommands.commands;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.Main;
import de.syranda.bettercommands.customclasses.BetterCommand;
import de.syranda.bettercommands.customclasses.SubCommand;
import de.syranda.bettercommands.customclasses.adc.Command;
import de.syranda.bettercommands.customclasses.adc.Help;
import de.syranda.bettercommands.customclasses.config.BetterCommandConfig;
import de.syranda.bettercommands.customclasses.config.ConfigRegistry;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import net.md_5.bungee.api.ChatColor;

@Command(value = "bettercommands", registerHelp = true, rootPermission = "bc", aliases = {"bc"})
public class BetterCommands {
		
	@Help("Command to print a command structure to a file")
	@Command(path = "print")
	public void printCommand(CommandSender sender, BetterCommand betterCommand) {
		
		File dir = new File(Main.getInstance().getDataFolder(), "//commands/" + (!betterCommand.getPlugin().equals("") ? betterCommand.getPlugin() + "/" : ""));				
		dir.mkdirs();
		
		File paste = new File(dir, "//" + betterCommand.getCommand() + ".txt");
		
		FileOutputStream fos = null;
		PrintWriter writer = null;
		
		try {
			
			fos = new FileOutputStream(paste);
			writer = new PrintWriter(fos);
			
			writer.flush();
			writer.println("/" + betterCommand.getCommand());					
			writer.println("\tAliases: [" + String.join(", ", betterCommand.getAliases()) + "]");
			writer.println("\tDescription: " + get(betterCommand.getTranslator().translate(false, betterCommand, betterCommand.getHelpMessage()).toLegacyText()));
			writer.println("\tRoot permission: " + (betterCommand.getRootPermission() != null ? betterCommand.getRootPermission() : "none"));	
			
			SubCommand root = betterCommand.getSubCommand("");
			
			if(root != null) {
				printSubCommand(writer, root, 1);
			
				if(betterCommand.getSubCommands().size() == 1)
					return;
			
			}
			
			writer.println("\tSub commands:");
			for(SubCommand subCommand : betterCommand.getSubCommands()) {
				
				if(subCommand.getPath().equals(""))
					continue;
				
				printSubCommand(writer, subCommand, 2);
				
				writer.println(" ");
				
			}
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			
			writer.close();
			try {
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		sender.sendMessage(":sCommand ':sh" + betterCommand.getName() + ":s' was printed");
						
	};
			
	private String get(String toGet) {
		return ChatColor.stripColor(toGet);
	}
	
	private void printSubCommand(PrintWriter writer, SubCommand subCommand, int tab) {
		
		String append = subCommand.repeatString("\t", tab);
		
		writer.println(append + (subCommand.getUsage(false).equals("") ? "Root command: " : subCommand.getUsage(false)));
		writer.println(append + "\tDescription: " + get(subCommand.getTranslator().translate(false, subCommand, subCommand.getHelpMessage().replace("\n", "\n" + append + "\t\t")).toLegacyText()));
		writer.println(append + "\tPermission: " + (subCommand.getPermission() != null ? subCommand.getPermission(): "none"));						
		
		List<String> notes = new ArrayList<>();
		
		if (subCommand.requiresExactPermission()) {
			notes.add(append + "\t\tRequires exact permision");
		}
		if (!subCommand.showInHelp()) {
			notes.add(append + "\t\tDoesn't show in help command");
		}
		if (subCommand.showOnPermission()) {
			notes.add(append + "\t\tDoesn't show in help if the sender doesn't have the required permission");
		}
		if (subCommand.isConversation()) {
			notes.add(append + "\t\tThis is a conversation command");
		}
		
		if (!notes.isEmpty()) {
			
			writer.println(append + "\tNotes:");
			
			for (String note : notes) {
				writer.println(note);
			}
			
		}
		
		if (!subCommand.getParameters().isEmpty()) {
		
			writer.println(append + "\tParameters:");
			
			for (int i = 0; i < subCommand.getParameters().size(); i++) {
				CommandParameter<?> parameter = subCommand.getParameters().get(i);
				writer.println(append + "\t\t" + parameter.getName() + (parameter.getParameterType() == ParameterType.OPTIONAL || parameter.getParameterType() == ParameterType.OPTIONAL_ARRAY ? " (Optional)" : "") + " - " + (subCommand.getParameterDescriptions().size() > i && subCommand.getParameterDescriptions().get(i) != null && !subCommand.getParameterDescriptions().get(i).equals("") ? subCommand.getParameterDescriptions().get(i) : "(No description provided)"));
			}
		}
		
	}
	
	@Help("Command to show a list of all configs")
	@Command(path = "config.list")
	public void configList(CommandSender sender) {		
		
		if (ConfigRegistry.getConfigs().isEmpty()) {
			sender.sendMessage(":fNo configs found");		
		} else {
			sender.sendMessage(":sThere are the following configs available");
			ConfigRegistry.getConfigs().forEach(config -> sender.sendMessage(":nConfig ':h" + config.getClass().getSimpleName() + ":n' of plugin ':h" + config.getPluginName() + ":n'"));
		}
	}
	
	@Help("Command to pring example content of a config")
	@Command(path = "config")
	public void printExampleContent(CommandSender sender, BetterCommandConfig config) {
		Main.sendExampleContent(sender, config);		
	}

}
