package de.syranda.bettercommands.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import de.syranda.bettercommands.customclasses.adc.Command;
import de.syranda.bettercommands.customclasses.adc.Help;
import de.syranda.bettercommands.customclasses.permissionhooks.HookRegistry;
import de.syranda.bettercommands.customclasses.permissionhooks.PermissionHook;

@Command(value = "permissionmanager", aliases = "pma", rootPermission = "permissionmanager", registerHelp = true)
public class PermissionManagerCommand {

	@Help("Opens the permission dialog")
	@Command(path = "options")
	public void options(Player player, String permission) {
		
		player.sendMessage("|:nPlease choose the action you want to perform:");
		player.sendMessage("|:nPermission: :sh" + permission);
		if(!HookRegistry.getUsedHookInstance().getGroups().isEmpty())
			player.sendMessage("|{:n[:sAdd to group:n]|RUN_COMMAND\\:/pma select " + permission + " add group} {:n[:fRemove from group:n]|RUN_COMMAND\\:/pma select " + permission + " remove group}");
		player.sendMessage("|{:n[:sAdd to player:n]|RUN_COMMAND\\:/pma select " + permission + " add player} {:n[:fRemove from player:n]|RUN_COMMAND\\:/pma select " + permission + " remove player}");
		
	}
	
	@Command(path = "select", showInHelp = false)
	public void select(Player player, String permission, String action, String who) {		
		
		PermissionHook hook = HookRegistry.getUsedHookInstance();
		
		List<String> players = Bukkit.getOnlinePlayers().stream().filter(p -> action.equals("add") ? !p.hasPermission(permission) : p.hasPermission(permission)).map(p -> p.getName()).collect(Collectors.toList());
		List<String> groups = hook.getGroups().stream().filter(group -> action.equals("add") ? !hook.canExecute(group, permission) : hook.canExecute(group, permission)).collect(Collectors.toList());	
		
		if((who.equals("player") && players.isEmpty()) || (who.equals("group") && groups.isEmpty()))
			player.sendMessage("E:Oops, there are no " + who + "s to " + action + " your permission");
		else {
			
			player.sendMessage("|:nPlease select a :sh" + who + ":n: ");
			player.sendMessage("|:nPermission: :sh" + permission);
			
			if(who.equals("player"))
				players.forEach(pl -> player.sendMessage("|{:m\u279C :n[" + (action.equals("remove") ? ":f" : ":s") + pl + ":n]|RUN_COMMAND\\:/pma selectpermission " + permission + " " + action + " player " + pl + "}"));
			else 
				groups.forEach(group -> player.sendMessage("|{:m\u279C :n[" + (action.equals("remove") ? ":f" : ":s") + group + ":n]|RUN_COMMAND\\:/pma selectpermission " + permission + " " + action + " group " + group + "}"));
		}		
	}
	
	@Command(path = "selectpermission", showInHelp = false)
	public void selectPermission(Player player, String permission, String action, String who, String target) {
		
		player.sendMessage("|:nPlease select the :shpermission :nyou want to " + action + ":");
		
		String[] split = permission.split("\\.");
		List<String> permissions = new ArrayList<>();
		
		for(int i = 0; i < split.length; i++) {
			if(i != 0)
				permissions.add(String.join(".", Arrays.copyOf(split, i)) + ".*");
			permissions.add(String.join(".", Arrays.copyOf(split, i + 1)));
		}
		
		permissions.forEach(per -> {
			player.sendMessage("|{:m\u279C :n[:sh" + per + ":n]|RUN_COMMAND\\:/pma action " + per + " " + action + " " + who + " " + target + "}");
		});
	}
	
	@Command(path = "action", showInHelp = false)
	public void action(Player player, String permission, String action, String who, String target) {
		
		if(action.equals("add"))
			if(who.equals("player")) {
				HookRegistry.getUsedHookInstance().addPermission(target, permission);
				player.sendMessage(":sAdded :npermission :sh" + permission + " :nto player :sh" + target);
			} else {
				HookRegistry.getUsedHookInstance().addGroupPermission(target, permission);
				player.sendMessage(":sAdded :npermission :sh" + permission + " :nto group :sh" + target);
			}
		else 
			if(who.equals("player")) {
				HookRegistry.getUsedHookInstance().removePermission(target, permission);
				player.sendMessage(":fRemoved :npermission :sh" + permission + " :nfrom player :sh" + target);
			} else {
				HookRegistry.getUsedHookInstance().removeGroupPermission(target, permission);
				player.sendMessage(":fRemoved :npermission :sh" + permission + " :nfrom group :sh" + target);
			}
			
	}
	
}
