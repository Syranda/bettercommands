package de.syranda.bettercommands.listener;

import java.util.Arrays;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerCommandEvent;

import de.syranda.bettercommands.Main;
import de.syranda.bettercommands.customclasses.BetterCommand;

public class ServerCommandListener implements Listener {

	public ServerCommandListener(Main c) {
		c.getServer().getPluginManager().registerEvents(this, c);
	}
	
	@EventHandler
	public void onEvent(ServerCommandEvent event) {
				
		BetterCommand command = BetterCommand.getBetterCommand(event.getCommand().split(" ")[0]);
		
		if(command == null)
			return;
			
		event.setCancelled(true);	
		command.execute(Main.getCommandSender(), event.getCommand().split(" ")[0], Arrays.copyOfRange(event.getCommand().split(" "), 1, event.getCommand().split(" ").length));
		
	}
	
}
