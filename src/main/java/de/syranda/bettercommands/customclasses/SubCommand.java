package de.syranda.bettercommands.customclasses;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permissible;
import org.bukkit.plugin.Plugin;

import de.syranda.bettercommands.Main;
import de.syranda.bettercommands.customclasses.adc.Array;
import de.syranda.bettercommands.customclasses.adc.Command;
import de.syranda.bettercommands.customclasses.adc.Conversation;
import de.syranda.bettercommands.customclasses.adc.Executer;
import de.syranda.bettercommands.customclasses.adc.Factory;
import de.syranda.bettercommands.customclasses.adc.Help;
import de.syranda.bettercommands.customclasses.adc.Optional;
import de.syranda.bettercommands.customclasses.adc.ParameterDescription;
import de.syranda.bettercommands.customclasses.config.BetterCommandConfig;
import de.syranda.bettercommands.customclasses.helper.parameters.BooleanParameter;
import de.syranda.bettercommands.customclasses.helper.validators.NegateValidator;
import de.syranda.bettercommands.customclasses.helper.validators.annotations.Negate;
import de.syranda.bettercommands.customclasses.messages.MessageTranslator;
import de.syranda.bettercommands.customclasses.nativeoverride.BetterCommandsConsoleCommandSender;
import de.syranda.bettercommands.customclasses.nativeoverride.CommandPacketReader;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterRegistry;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;
import de.syranda.bettercommands.customclasses.parameter.validators.SubValidator;
import de.syranda.bettercommands.customclasses.parameter.validators.SubValidatorPayload;
import de.syranda.bettercommands.customclasses.parameter.validators.ValidatorRegistry;
import net.md_5.bungee.api.ChatColor;

public class SubCommand implements BetterCommandConfig {

	public static SubCommand getSubCommand(Class<?> clazz) {
		
		Method method = Stream.of(Thread.currentThread().getStackTrace())
				.filter(stackTrace -> Stream.of(clazz.getMethods()).filter(meth -> meth.getName().equals(stackTrace.getMethodName())).findAny().orElse(null) != null 
				&& Stream.of(clazz.getMethods()).filter(meth -> meth.getName().equals(stackTrace.getMethodName())).findAny().orElse(null).isAnnotationPresent(Command.class))
				.findAny().map(name -> Stream.of(clazz.getMethods()).filter(meth -> meth.getName().equals(name.getMethodName())).findAny().orElse(null)).orElse(null);
		
		if (method == null) {
			throw new RuntimeException("Cannot trace the command's origin. Maybe you're using this method wrong?");
		}
		
		String commandName;
		
		if (method.getAnnotation(Command.class).value().equals("none") && clazz.isAnnotationPresent(Command.class)) {
			commandName = clazz.getAnnotation(Command.class).value().equals("none") ? clazz.getSimpleName().toLowerCase() : clazz.getAnnotation(Command.class).value();
		} else { 
			commandName = method.getAnnotation(Command.class).value().equals("none") ? method.getName() : method.getAnnotation(Command.class).value();
		}
		
		return BetterCommand.getBetterCommand(commandName).getSubCommand(method.getAnnotation(Command.class).path());
	}
	
	private BetterCommand command;
	private String path;
	private String permission;

	private boolean isPlayerOnly = false;
	
	private String helpMessage;
	private String permissionDeniedMessage;
	private String syntaxErrorMessage;
	private String invalidSenderMessage;	
	private String noActionMessage;
	private String enumErrorMessage;
	
	private String prefix;
	private ChatColor normalColor;
	private ChatColor subNormalColor;
	private ChatColor successColor;
	private ChatColor failedColor;
	private ChatColor highlightColor;
	private ChatColor subHighlightColor;
	private ChatColor minorColor;	
	
	private boolean showInHelp;
	private boolean showPermission;
	private boolean showOnPermission;
	private boolean appendPrefixOnError;
	private boolean requiresExactPermission;
	private boolean hasPacketInjector;
	
	private boolean isConversation;
	private boolean isGlobalInjection;
		
	private List<CommandParameter<?>> parameters;
	private List<SubValidator> senderValidators;
	private MessageTranslator translator;
	
	private List<String> parameterDescriptions = new ArrayList<>();
	private HashMap<String, String> flags = new HashMap<>();
	
	private Method exe;
	private Object invoke;
	
	public SubCommand(BetterCommand command, String path) {
				
		this.command = command;
		this.path = path;
		
		if (!hasCorrectFormedPath()) {
			throw new RuntimeException("Error while loading " + toString() + ". The sub command has a bad formatted path");
		}
			
		parameters = new ArrayList<>();
		senderValidators = new ArrayList<>();
		
		if (command == null) {
			return;
		}
		
		if (getCommand().getRootPermission() != null) {
			this.permission = getCommand().getRootPermission() + (!path.equals("") ? "." + path.toLowerCase() : "");
		}
		
		this.setPrefix(command.getPrefix());
		this.setNormalColor(command.getNormalColor());
		this.setSubNormalColor(command.getSubNormalColor());
		this.setSuccessColor(command.getSuccessColor());
		this.setFailedColor(command.getFailedColor());
		this.setHighlightColor(command.getHighlightColor());
		this.setSubHighlightColor(command.getSubHighlightColor());
		this.setMinorColor(command.getMinorColor());
		
		this.setAppendPrefixOnError(command.appendPrefixOnError());
		this.setTranslator(command.getTranslator());
		
		this.setHelpMessage(command.getHelpMessage());		
		this.setPermissionDeniedMessage(command.getPermissionDeniedMessage());
		this.setSyntaxErrorMessage(command.getSyntaxErrorMessage());
		this.setInvalidSenderMessage(command.getInvalidSenderMessage());
		this.setNoActionMessage(command.getNoActionMessage());
		this.setEnumErrorMessage(command.getEnumErrorMessage());
		
		this.setShowInHelp(command.showInHelp());
		this.setShowPermission(command.showPermision());
		this.getValidators().addAll(command.getValidators());
		this.setShowOnPermission(command.showOnPermission());
		this.setConversation(command.isConversation());
		this.setRequiresExactPermission(command.requiresExactPermission());
		this.setHasPacketInjector(command.hasPacketInjector());		
				
		this.setGlobalInjection(false);
					
		this.registerMethod(this, Stream.of(this.getClass().getDeclaredMethods()).filter(method -> method.isAnnotationPresent(Executer.class)).findAny().orElse(null));
		
	}
	
	public SubCommand(BetterCommand command) {
		this(command, "");
	}
	
	public final BetterCommand getCommand() {
		return command;
	}
	
	public final String getPath() {
		return path;
	}	
	public final String[] getPathArray() {
		return Stream.of(getPath().split("\\.")).filter(part -> !part.equals("")).toArray(String[]::new);
	}	
	public final String getPermission() {
		return permission;
	}
	public final void setPermission(String permission) {
		this.permission = permission;
	}	
	public final void clearPermission() {
		this.permission = null;
	}
	public final void setPlayerOnly(boolean isPlayerOnly) {
		this.isPlayerOnly = isPlayerOnly;
	}
	public final boolean isPlayerOnly() {
		return isPlayerOnly;
	}
		
	public final void addParameter(CommandParameter<?> parameter) {
		
		try {
		
			if (!this.isConversation()) {
			
				if (!this.getParameters().isEmpty()) {
					
					ParameterType lastType = this.parameters.get(this.parameters.size() - 1).getParameterType();
					
					if (lastType == ParameterType.CONSTRAINT_ARRAY || lastType == ParameterType.OPTIONAL_ARRAY) {
						throw new RuntimeException("Error while loading " + toString() + ". Cannot register parameter after an array parameter");
					} else if (lastType == ParameterType.OPTIONAL && (parameter.getParameterType() == ParameterType.CONSTRAINT || parameter.getParameterType() == ParameterType.CONSTRAINT_ARRAY)) {
						throw new RuntimeException("Error while loading " + toString() + ". Cannot register a contraint parameter after an optional parameter");
					} else {
						for (CommandParameter<?> commandParameter : parameters) {
							if (commandParameter.getName().equalsIgnoreCase(parameter.getName())) {
								throw new RuntimeException("Error while loading " + toString() + ". Cannot register paremeters with the same name");
							}
						}
					}
				}
				
			} else if (parameter.getParameterType() == ParameterType.CONSTRAINT_ARRAY) {
				parameter.setParameterType(ParameterType.CONSTRAINT);
			} else if (parameter.getParameterType() == ParameterType.OPTIONAL_ARRAY) {
				parameter.setParameterType(ParameterType.OPTIONAL);			
			}
	
			this.parameters.add(parameter);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public final void addParameter(String name, ParameterType type) {		
		this.addParameter(new CommandParameter<Object>(name, type) {
			@Override
			public ValidationPayload<Object> validate(CommandSender sender, CommandValue<Object> value,
					ParameterSet parameterSet) {
				return new ValidationPayload<>().successWithObject(value.asObject());
			}
		});		
	}
		
	public final List<CommandParameter<?>> getParameters() {
		return this.parameters;
	}
	
	@SuppressWarnings({ "rawtypes" })
	public final ExecutionPayload internalExecute(CommandSender sender, String[] args) {
		
		ExecutionPayload payload = new ExecutionPayload(this);
		
		if (!this.hasPermission(sender)) {
			return payload.failWithMessage(this.getPermissionDeniedMessage());
		}
		
		args = this.formatFlagSyntax(args);
		
		if (!this.isSuitable(args)) {
			payload.failWithMessage(this.getSyntaxErrorMessage());
			payload.addVar(this.getUsage(true));
			return payload;
		}
		
		if (this.isPlayerOnly() && !(sender instanceof Player)) {
			return payload.failWithMessage(this.getInvalidSenderMessage());
		}
		
		for (SubValidator subValidator : this.senderValidators) {
			SubValidatorPayload validationPayload = subValidator.validate(sender, null, null);
			if (!validationPayload.isSuccess()) {
				validationPayload.getVars().forEach(x -> payload.addVar(x));
				return payload.failWithMessage(validationPayload.getErrorMessage());
			}
		}
		
		ParameterSet set = this.getParameterSet(sender, args);	
				
		CommandValue[] valueArray = set.getParameters();
		
		for (int i = 0; i < valueArray.length; i++) {
			CommandValue<?> value = valueArray[i];
			ValidationPayload<?> validationPayload = value.validate(sender, new ParameterSet(this, Arrays.copyOf(valueArray, i)));
			if (validationPayload.hasFailed()) {
				validationPayload.getVars().forEach(var -> payload.addVar(var));
				return payload.failWithMessage(validationPayload.getErrorMessage());
			}
		}
		
		if (exe != null) {
			for (int i = 0; i < this.getParameters().size(); i++) {
				Parameter parameter = exe.getParameters()[i + 1];
				if (set.get(parameter.getName()).isNull() && parameter.isAnnotationPresent(Optional.class)) {
					set.setDefault(sender, this.parameters.get(i), parameter.getDeclaredAnnotation(Optional.class).value());
				}
			}
		}
								
		final CommandPacketReader reader = new CommandPacketReader(sender instanceof Player ? (Player) sender : null, this);
		
		if (this.hasPacketInjector()) {			
			 BetterCommandsConsoleCommandSender.setCurrentSubCommand(this);
			 BetterCommandsConsoleCommandSender.setReader(reader);
		}
		
		if (this.exe != null) {
			
			Object[] paras = new Object[exe.getParameterCount()];
			paras[0] = this.isPlayerOnly() ? (Player) sender : sender;
			
			Parameter[] parameters = this.exe.getParameters();
			
			for (int i = 1; i < parameters.length; i++) {
				paras[i] = set.get(parameters[i].getName()).asValidatedObject();
				if (paras[i] instanceof Player) {
					reader.getAdditionalPlayers().add((Player) paras[i]);
				}
			}
			
			if (this.hasPacketInjector()) {
				if (this.isGlobalInjection()) {
					Bukkit.getOnlinePlayers().forEach(player -> reader.getAdditionalPlayers().add(player));
				}
				reader.inject();
			}
			
			try {
				this.exe.setAccessible(true);
				this.exe.invoke(this.getInvoke(), paras);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
			
		} else {
			if (this.hasPacketInjector()) {
				if (this.isGlobalInjection()) {
					Bukkit.getOnlinePlayers().forEach(player -> reader.getAdditionalPlayers().add(player));
				}
				reader.inject();
			}
			this.execute(sender, set);
		}
		
		if (this.hasPacketInjector()) {
			Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> this.clearTranslator(reader), 1);
		}
		
		return payload;
		
	}
	
	private final String[] formatFlagSyntax(String[] args) { 
		
		if (args.length == 0) {
			return args;
		}
			
		List<String> ret = new ArrayList<>();
		ret.addAll(Stream.of(Arrays.copyOf(args, this.getPathArray().length)).collect(Collectors.toList()));
		IntStream.range(0, (int) Stream.of(args).filter(part -> !this.flags.containsKey(part)).count()).forEach(x -> ret.add("\\o"));
		
		int flags = 0;
		for (int i = this.getPathArray().length; i < args.length; i++) {
			CommandParameter<?> parameter = this.getParameterForFlag(args[i]);
			if (parameter != null) {
				int index = this.parameters.indexOf(parameter) + this.getPathArray().length - this.getInlineLength();
				
				if (parameter instanceof BooleanParameter) {					
					ret.set(index, "true");
				} else if (i + 1 < args.length) {
					if (parameter.getParameterType().isArray()) {
						String add = Stream.of(args).limit(this.getNextFlagParameterIndex(i, args)).skip(i + 1).collect(Collectors.joining(" "));
						ret.set(index, add);
						i += add.split(" ").length;
					} else {
						ret.set(index, args[i + 1]);
						i++;
					}
				}
				flags++;
			} else {
				int parameterIndex = i - this.getPathArray().length + this.getInlineLength() - flags;
				parameter = parameterIndex < this.parameters.size() ? this.parameters.get(parameterIndex) : null;
				for (int index = 0; index < ret.size(); index++) {
					if (ret.get(index).equals("\\o")) {						
						if (parameter != null && parameter.getParameterType().isArray()) {
							String add = Stream.of(args).limit(this.getNextFlagParameterIndex(i, args)).skip(i).collect(Collectors.joining(" "));
							ret.set(index, add);
							i += add.split(" ").length - 1;
						} else {
							ret.set(index, args[i]);
						}
						break;
					}
				}
			}
		}
		
		for (int i = ret.size() - 1; i > 0; i--) {
			if (ret.get(i) == null || ret.get(i).equals("\\o")) {
				ret.remove(i);
			} else {
				break;
			}
		}
		
		return ret.stream().map(part -> part == null || part.equals("\\o") ? "null" : part).toArray(String[]::new);
		
	}
	
	private int getNextFlagParameterIndex(int offset, String[] args) {
		int nextFlagIndex;
		for (nextFlagIndex = offset + 1; nextFlagIndex < args.length; nextFlagIndex++) {
			if (this.flags.containsKey(args[nextFlagIndex])) {
				break;
			}
		}
		return nextFlagIndex;
	}

	public final boolean isSuitable(String[] args) {
		return this.matchesPath(args) && args.length <= this.getMaxLength() && args.length >= this.getMinLength();		
	}
	
	private final int getMinLength() {
		return this.getPathArray().length - this.getInlineLength() 
				+ (int) this.parameters.stream().filter(parameter -> !parameter.getParameterType().isOptional()).count();
	}
	
	private final int getMaxLength() {
		return this.getPathArray().length - this.getInlineLength()
				+ (int)(this.parameters.stream().anyMatch(parameter -> parameter.getParameterType().isArray()) 
																						? Integer.MAX_VALUE - 1000 
																						: this.parameters.size());
	}
	
	public final int getInlineLength() {
		return (int) Stream.of(this.getPathArray()).filter(part -> part.equals("%")).count();
	}
	
	private final void clearTranslator(CommandPacketReader reader) {
		if (!this.hasPacketInjector() || reader == null) {
			return;
		}
		reader.remove();
		BetterCommandsConsoleCommandSender.setCurrentSubCommand(null);
		BetterCommandsConsoleCommandSender.setReader(null);
	}
	
	public final boolean hasPermission(Permissible permissible) {
		if (this.getPermission() == null) {
			return true;
		}
		return this.requiresExactPermission() 
				? permissible.getEffectivePermissions().stream().anyMatch(entry -> entry.getPermission().equals(this.getPermission()))
				: permissible.hasPermission(this.getPermission());
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ParameterSet getParameterSet(CommandSender sender, String[] args) {
		List<CommandValue<?>> sortOut = new ArrayList<>();
		args = this.formatFlagSyntax(args);
		for (int i = 0; i < args.length; i++) {
			if (i >= this.getPathArray().length || this.getPathArray()[i].equals("%")) {
				CommandParameter<?> parameter = sortOut.size() < this.parameters.size() 
						? this.parameters.get(sortOut.size()) 
						: this.parameters.get(this.parameters.size() - 1);
									
				if (parameter.getParameterType().isArray()) {
					CommandValue value = new CommandValue(String.join(" ", Arrays.copyOfRange(args, i, args.length)), parameter);
					value.validate(sender, this.getParameterSet(sender, Arrays.copyOf(args, i)));
					sortOut.add(value);		
					break;
				} else {
					CommandValue value = new CommandValue(args[i], parameter);
					value.validate(sender, this.getParameterSet(sender, Arrays.copyOf(args, i)));
					sortOut.add(value);				
				}
			}				
		}
		
		return new ParameterSet(this, sortOut.stream().toArray(CommandValue[]::new));
	}
	
	@SuppressWarnings("rawtypes")
	public final void registerMethod(Object invoke, Method exe) {
		
		if (exe == null) {
			return;
		}
		
		if(invoke == null) {
			throw new IllegalArgumentException("Invoking object cannot be null");
		}
		
		Parameter[] parameters = exe.getParameters();
		
		if (!(CommandSender.class.isAssignableFrom(parameters[0].getType()))) {
			try {
				throw new RuntimeException("The first parameter of your command method needs to be a 'CommandSender'");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		isPlayerOnly = Player.class.isAssignableFrom(parameters[0].getType());
				
		for (Annotation annotation : parameters[0].getDeclaredAnnotations()) {
			SubValidator validator = ValidatorRegistry.getValidator(annotation);
			if (validator != null) {
				this.addValidator(validator);
			} else {
				try {
					throw new RuntimeException("Error loading " + toString() + ". Annotation '" + annotation.annotationType().getSimpleName() + "' is neither functional nor a sub validator");
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}
			}
		}
		
		if (exe.isAnnotationPresent(Help.class)) {
			setHelpMessage(exe.getDeclaredAnnotation(Help.class).value());
		}
		
		if (exe.isAnnotationPresent(Conversation.class)) {
			setConversation(exe.getDeclaredAnnotation(Conversation.class).value());
		}
		
		Command command = exe.getAnnotation(Command.class);
			
		if (command != null) {
			if (!command.inherit()) {
				setRequiresExactPermission(command.requiresExplicitPermission());			
				setShowInHelp(command.showInHelp());
				setShowOnPermission(command.showOnPermission());
				setShowPermission(command.showPermission());
				setAppendPrefixOnError(command.appendPrefixOnError());
				setHasPacketInjector(command.hasReader());
			}		
			
			if (!command.prefix().equals("none")) {
				setPrefix(exe.getAnnotation(Command.class).prefix());
			}
			if (!command.permission().equals("none")) {
				setPermission(command.permission());
			}
			setGlobalInjection(command.globalInjection());
		}

		if (exe.isAnnotationPresent(ParameterDescription.class)) {
			setParameterDescription(exe.getAnnotation(ParameterDescription.class).value());
		}
		
		for (int i = 1; i < exe.getParameters().length; i++) {
			
			Parameter parameter = parameters[i];
			
			if (parameter.getType().isPrimitive()) {
				try {
					throw new RuntimeException("Don't use primitive variable types ('" + parameter.getType().getSimpleName() + "'). User their wrapper counterpart instead. (Command: " + this.toString() + ")");
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}
			}
				
			ParameterType type = ParameterType.CONSTRAINT;
			
			if (parameter.isAnnotationPresent(Optional.class)) {
				type = ParameterType.OPTIONAL;
				Optional optional = parameter.getDeclaredAnnotation(Optional.class);
				if (!optional.flag().equals("null")) {
					this.flags.put("-" + optional.flag(), parameter.getName());
				}				
			}
			
			if (parameter.isAnnotationPresent(Array.class)) {
				type = type == ParameterType.CONSTRAINT ? ParameterType.CONSTRAINT_ARRAY : ParameterType.OPTIONAL_ARRAY;										
			}
			
			CommandValue<?>[] values = null;
			
			if (parameter.isAnnotationPresent(Factory.class)) {
				try {
					String[] arguments = (String[]) parameter.getDeclaredAnnotation(Factory.class).annotationType().getDeclaredMethod("value").invoke(parameter.getDeclaredAnnotation(Factory.class));
					values = Stream.of(arguments).map(arg -> new CommandValue(arg)).collect(Collectors.toList()).toArray(new CommandValue[arguments.length]);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException e) {
				}								
			}
					
			CommandParameter<?> commandParameter = ParameterRegistry.getParameterForType(parameter.getName(), type, parameter.getType(), values);
					
			if (commandParameter == null && parameter.getType().isEnum()) {
				commandParameter = new CommandParameter(parameter.getName(), type) {					
					
					public List<String> getTabComplete(CommandSender sender, ParameterSet parameterSet) {
						return Stream.of(parameter.getType().getEnumConstants()).map(en -> en.toString()).collect(Collectors.toList());
					}

					@Override
					public ValidationPayload validate(CommandSender sender, CommandValue value, ParameterSet parameterSet) {
						Enum ret = (Enum) Stream.of(parameter.getType().getEnumConstants()).filter(en -> en.toString().equalsIgnoreCase(value.asString())).findAny().orElse(null);
						
						return ret == null 
								? new ValidationPayload<>().failWithMessage(EnumMessages.getEnumMessage(parameter.getType(), SubCommand.this, value.asString())) 
								: new ValidationPayload<>().successWithObject(ret);
						
					};
					
				};
			}
			
						
			if (commandParameter == null) {
				throw new RuntimeException("No command parameter found for type '" + parameter.getType().getSimpleName() + "'");
			} else {				
				
				if (parameter.isAnnotationPresent(Negate.class)) {
					commandParameter.addSubValidator(new NegateValidator(parameter.getDeclaredAnnotation(Negate.class).value()));					
				}
				
				for (Annotation annotation : parameter.getDeclaredAnnotations()) {
					SubValidator validator = ValidatorRegistry.getValidator(annotation);
					if (validator != null) {
						commandParameter.addSubValidator(validator);
					} else if(!annotation.annotationType().equals(Array.class)
							&& !annotation.annotationType().equals(Optional.class)
							&& !annotation.annotationType().equals(Negate.class)) {
						try {
							throw new RuntimeException("Error loading sub command " + toString() + ". Annotation '" + annotation.annotationType().getName() + "' is neither functional nor a sub validator");
						} catch (Exception e) {
							e.printStackTrace();
							continue;
						}
						
					}
				}
				
				addParameter(commandParameter);
			}
		
		}		
		
		this.exe = exe;
		this.invoke = invoke;
		
	}
	
	public void register() {
	}
	
	public void execute(CommandSender sender, ParameterSet parameterSet) {
	}
	
	public final String getUsage(boolean appendCommand) {
		StringBuilder builder = new StringBuilder();
		if (appendCommand) {
			builder.append("/" + this.getCommand().getName());
		}
		
		String[] path = this.getPathArray();
		
		if (path.length > 0) {
			if (appendCommand) {
				builder.append(" ");
			}
			
			List<String> pathList = new ArrayList<String>();
			int parameterIndex = 0;
			
			for (String part : path) {				
				if (part.equals("%")) {
					pathList.add(this.formatUsage(this.getParameters().get(parameterIndex)));
					parameterIndex++;
				} else {
					pathList.add(part);
				}
			}
			builder.append(pathList.stream().collect(Collectors.joining(" ")));			
		}
		
		if (this.parameters.size() - this.getInlineLength() > 0) {	
			builder.append(" ");			
			builder.append(this.parameters.stream().skip(this.getInlineLength()).map(parameter -> this.formatUsage(parameter)).collect(Collectors.joining(" ")));
		}
		return builder.toString();
	}
	
	private String formatUsage(CommandParameter<?> parameter) {
		String name = parameter.getName().toLowerCase();
		switch (parameter.getParameterType()) {
		case CONSTRAINT:
			return "<" + name + ">";
		case CONSTRAINT_ARRAY:
			return "<" + name + "...>";
		case OPTIONAL:
			return "[" + name + "]";
		case OPTIONAL_ARRAY:
			return "[" + name + "...]";
		default:
			return "";
		}
	}
	
	public final String getHelpMessage() {
		return helpMessage;
	}
	public final void setHelpMessage(String helpMessage) {
		this.helpMessage = helpMessage;			
	}

	public final String getPermissionDeniedMessage() {
		return permissionDeniedMessage;
	}
	public final void setPermissionDeniedMessage(String permissionDeniedMessage) {
		this.permissionDeniedMessage = permissionDeniedMessage;
	}

	public final String getSyntaxErrorMessage() {
		return syntaxErrorMessage;
	}
	public final void setSyntaxErrorMessage(String syntaxErrorMessage) {
		this.syntaxErrorMessage = syntaxErrorMessage;
	}
	
	public final String getInvalidSenderMessage() {
		return invalidSenderMessage;
	}
	public final void setInvalidSenderMessage(String invalidSenderMessage) {
		this.invalidSenderMessage = invalidSenderMessage;
	}

	public final ChatColor getNormalColor() {
		return normalColor;
	}
	public final void setNormalColor(ChatColor normalColor) {
		this.normalColor = normalColor;
	}

	public final ChatColor getSuccessColor() {
		return successColor;
	}
	public final void setSuccessColor(ChatColor successColor) {
		this.successColor = successColor;
	}

	public final ChatColor getFailedColor() {
		return failedColor;
	}
	public final void setFailedColor(ChatColor failedColor) {
		this.failedColor = failedColor;
	}
	
	public final ChatColor getHighlightColor() {
		return highlightColor;
	}
	public final void setHighlightColor(ChatColor highlightColor) {
		this.highlightColor = highlightColor;
	}
	
	public final ChatColor getMinorColor() {
		return minorColor;
	}
	public final void setMinorColor(ChatColor minorColor) {
		this.minorColor = minorColor;
	}

	public final ChatColor getSubNormalColor() {
		return subNormalColor;
	}
	public final void setSubNormalColor(ChatColor subNormalColor) {
		this.subNormalColor = subNormalColor;
	}

	public final ChatColor getSubHighlightColor() {
		return subHighlightColor;
	}
	public final void setSubHighlightColor(ChatColor subHighlightColor) {
		this.subHighlightColor = subHighlightColor;
	}
	
	public final String getPrefix() {
		return prefix;
	}
	public final void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public final boolean showInHelp() {
		return showInHelp;
	}
	public final void setShowInHelp(boolean showInHelp) {
		this.showInHelp = showInHelp;
	}

	public final boolean showPermision() {
		return showPermission;
	}
	public final void setShowPermission(boolean showPermission) {
		this.showPermission = showPermission;
	}
	
	@Override
	public final List<SubValidator> getValidators() {
		return this.senderValidators;
	}
	
	public final void setValidators(List<SubValidator> validators) {
		this.senderValidators = validators;
	}
	
	public final void clearValidators() {
		this.senderValidators.clear();		
	}
	
	@Override
	public final MessageTranslator getTranslator() {
		return translator;
	}
	
	public final void setTranslator(MessageTranslator translator) {
		this.translator = translator;
	}
	
	@Override
	public final boolean showOnPermission() {
		return this.showOnPermission;
	}
	
	public final void setShowOnPermission(boolean showOnPermission) {
		this.showOnPermission = showOnPermission;
	}
	
	@Override
	public final boolean appendPrefixOnError() {
		return this.appendPrefixOnError;
	}
	
	public final void setAppendPrefixOnError(boolean appendPrefixOnError) {
		this.appendPrefixOnError = appendPrefixOnError;
	}
	
	@Override
	public final boolean requiresExactPermission() {
		return this.requiresExactPermission;
	}
	
	public final void setRequiresExactPermission(boolean requiresExactPermission) {
		this.requiresExactPermission = requiresExactPermission;
	}
	
	public final void sendMessage(CommandSender sender, String message, Object... arguments) {
		if(sender instanceof Player) {
			((Player) sender).spigot().sendMessage(this.getTranslator().translate(this, message, arguments));		
	 	} else {
			sender.sendMessage(this.getTranslator().translate(this, message, arguments).toLegacyText());
		}
	}
	
	public final void sendErrorMessage(CommandSender sender, String message, Object... arguments) {
		if (sender instanceof Player) {
			((Player) sender).spigot().sendMessage(this.getTranslator().translateError(this, message, arguments));		
		} else {
			sender.sendMessage(this.getTranslator().translateError(this, message, arguments).toLegacyText());
		}
	}
	
	public final void sendMessage(CommandSender sender, String message) {
		this.sendMessage(sender, message, new Object[0]);
	}
	
	public final void sendErrorMessage(CommandSender sender, String message) {
		this.sendErrorMessage(sender, message, new Object[0]);
	}

	@Override
	public final boolean isConversation() {
		return this.isConversation;
	}
	
	public final void setConversation(boolean isConversation) {
		
		if (!this.getParameters().isEmpty()) {
			throw new RuntimeException("Error while loading " + this.toString() + ". Cannot change conversation status if the command already contains any parameters");
		}
		
		this.isConversation = isConversation;		
	}
		
	public final boolean matchesPath(String[] args) {
				
		String[] pathArray = this.getPathArray();
		
		if (args.length < pathArray.length) {
			return false;			
		}
		
		for (int i = 0; i < pathArray.length; i++) {
			
			String arg = args[i];
			
			if (i >= args.length) {
				break;
			}
			
			if (pathArray[i].equals("%")) {
				continue;
			}
			
			if (!pathArray[i].equals(arg) 
					&& !(pathArray[i].contains("|") && !pathArray[i].startsWith("|") && !pathArray[i].endsWith("|") && Stream.of(pathArray[i].split("\\|")).anyMatch(part -> part.equals(arg)))) {
				return false;
			} 
			
		}

		return true;
		
	}
	
	public final boolean hasCorrectFormedPath() {		
		return this.getPath().equals("") || this.getPath().matches("(([^\\.\\s\\%]+|%)(\\.([^\\.\\s\\%]+|%))*\\.)?[^\\s\\.\\%]+");
	}
	
	public final void addValidator(SubValidator validator) {
		this.senderValidators.add(validator);
	}
	
	@Override
	public final Plugin getOwner() {
		return this.getCommand().getOwner();
	}
	
	public final Object getInvoke() {
		return this.invoke;
	}
	
	public final Method getExe() {
		return this.exe;
	}
	
	public final String getSuggestSyntax() {
		
		int inlineIndex = 0;
		
		String[] path = this.getPathArray().clone();
		
		for (int i = 0; i < path.length; i++) {
			if (path[i].equals("%")) {
				path[i] = "<" + this.parameters.get(inlineIndex).getName() + ">";
				inlineIndex++;
			}
		}
		
		return "/" + this.getCommand().getCommand() + " " + String.join(" ", path);		
	}
	
	public final boolean isGlobalInjection() {
		return this.isGlobalInjection;
	}
	
	public final void setGlobalInjection(boolean isGlobalInjection) {
		this.isGlobalInjection = isGlobalInjection;
	}
	
	public final List<String> getParameterDescriptions() {
		return this.parameterDescriptions;
	}
	
	public final void setParameterDescription(String... description) {
		this.parameterDescriptions = Arrays.asList(description);
	}
	
	@Override
	public String toString() {
		return "SubCommand [Main: " + this.getCommand().getCommand() + " | Path: " + this.getPath() + "]";
	}
	
	public final void setNoActionMessage(String noActionMessage) {
		this.noActionMessage = noActionMessage;
	}
	
	@Override
	public final String getNoActionMessage() {
		return this.noActionMessage;
	}
	
	@Override
	public final boolean hasPacketInjector() {
		return this.hasPacketInjector;
	}
	
	public final void setHasPacketInjector(boolean hasPacketInjector) {
		this.hasPacketInjector = hasPacketInjector;
	}

	public final String repeatString(String string, int amount) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < amount; i++) {
			builder.append(string);
		}
		return builder.toString();
	}
	
	public final boolean showToSender(CommandSender sender) {
		
		if (!this.showInHelp()) {
			return false;
		}
		
		if (!(sender instanceof Player)) {
			return true;
		}
		
		return this.showOnPermission() ? this.hasPermission(sender) : true;		
	}
	
	public final List<String> getTabComplete(CommandSender sender, String[] args) {
		
		List<String> ret = new ArrayList<>();
		String[] path = this.getPathArray();
		
		String[] pathArray = this.getPathArray();
		
		for (int i = 0; i < Math.min(args.length - 1, this.getPathArray().length); i++) {
			
			String arg = args[i];
			
			if (!pathArray[i].equals(arg) && !pathArray[i].equals("%")
					&& !(pathArray[i].contains("|") && !pathArray[i].startsWith("|") && !pathArray[i].endsWith("|") && Stream.of(pathArray[i].split("\\|")).anyMatch(part -> part.equals(arg)))) {
				return ret;
			}
		}
		
		int currentIndex = args.length - 1;
		
		if (currentIndex < path.length) {
			if (path[currentIndex].equals("%")) {				
				ret.addAll(this.getParameters().get((int) Stream.of(this.getPathArray()).limit(currentIndex).filter(part -> part.equals("%")).count())
						.getInternalTabComplete(sender, args[currentIndex], currentIndex > 0 
								? this.getParameterSet(sender, Arrays.copyOf(args, currentIndex - 1)) 
										: null));	
			} else {
				String add = path[currentIndex];
				if (add.contains("|") && !add.startsWith("|") && !add.endsWith("|")) {
					add = add.split("\\|")[0];
				}
				ret.add(add);
			}
		} else { 
			CommandParameter<?> parameter = null;
			for (int i = currentIndex; i >= 0; i--) {
				if (this.flags.containsKey(args[i])) {
					CommandParameter<?> flagParamter = this.getParameterForFlag(args[i]);
					parameter = flagParamter.getParameterType().isArray() || i == currentIndex - 1 ? flagParamter : null;
					break;
				}
			}
			
			if (parameter == null) {
				int formerFlags = (int) Stream.of(args).filter(part -> this.flags.containsKey(part)).count();
				parameter = currentIndex - path.length + this.getInlineLength() - formerFlags < this.getParameters().size() 
						? this.getParameters().get(currentIndex - path.length + this.getInlineLength() - formerFlags)
						: !this.getParameters().isEmpty() && this.getParameters().get(this.getParameters().size() - 1).getParameterType().isArray() 
								? this.getParameters().get(this.getParameters().size() - 1) 
								: null;
			}
			
			if (parameter != null) {
				ret.addAll(parameter.getInternalTabComplete(sender, args[currentIndex], currentIndex > 0 ? this.getParameterSet(sender, Arrays.copyOf(args, currentIndex)) : null));
			}
		}		
		
		return ret;
		
	}
	
	@Override
	public final String getEnumErrorMessage() {
		return this.enumErrorMessage;
	}
	
	public final void setEnumErrorMessage(String enumErrorMessage) {
		this.enumErrorMessage = enumErrorMessage;
	}
	
	public final CommandParameter<?> getParameterForFlag(String flag) {
		if (!this.flags.containsKey(flag)) {
			return null;
		}
		return this.getParameters().stream().filter(paramter -> paramter.getName().equals(this.flags.get(flag))).findAny().orElse(null);
	}
	
}

