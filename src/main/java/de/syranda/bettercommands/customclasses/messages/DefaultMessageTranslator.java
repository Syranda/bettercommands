package de.syranda.bettercommands.customclasses.messages;

import de.syranda.bettercommands.customclasses.config.BetterCommandConfig;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;

public class DefaultMessageTranslator implements MessageTranslator {

	@Override
	public TextComponent translate(boolean prefix, BetterCommandConfig config, String message, Object... arguments) {
	
		if(message.startsWith("E:"))
			return translateError(config, message.substring(2), arguments);
		
		message = (message.startsWith(":f") ? ":f" : "") + CommandConfig.get(config.getOwner()).translate(message.startsWith(":f") ? message.substring(2) : message, arguments);
		String ret = (prefix ? config.getPrefix() : "") + ":r" + ChatColor.translateAlternateColorCodes('&', message);
		ret = translateDefaultColors(ret, config);
		
		StringBuilder builder = new StringBuilder(ret);
		char lastColor = 0;
		
		for(int i = 0; i < builder.toString().length() - 1 && i < 1000; i++) 
			if(builder.charAt(i) == ChatColor.COLOR_CHAR) 
				lastColor = builder.charAt(i + 1);	
			else if(builder.charAt(i) == ' ' && builder.charAt(i + 1) != ' ' && builder.charAt(i + 1) != ChatColor.COLOR_CHAR && lastColor != 0) {
				builder.insert(i + 1, "" + ChatColor.COLOR_CHAR + lastColor);
				i+=3;
			}
		
		ret = builder.toString();		
		ret = ret.replaceAll("\t", "      ");
		
		return translateComponentPattern(ret);
		
	}
	
}
