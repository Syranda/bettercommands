package de.syranda.bettercommands.customclasses.messages;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.nativeoverride.BetterCommandsConsoleCommandSender;
import de.syranda.bettercommands.customclasses.nativeoverride.CommandPacketReader;

public class TranslatorDisabler {

	private CommandSender owner;
	private CommandPacketReader reader;
	
	public TranslatorDisabler(CommandSender owner, CommandPacketReader reader) {
		this.owner = owner;
		this.reader = reader;
	}
	
	public void disable() {
		
		if (owner instanceof BetterCommandsConsoleCommandSender) {
			BetterCommandsConsoleCommandSender.setCurrentSubCommand(null);			
		}
		
		if (reader != null) {
			reader.remove();
		}
		
	}
	
	public void restore() {
		
		if (owner instanceof BetterCommandsConsoleCommandSender) {
			BetterCommandsConsoleCommandSender.setCurrentSubCommand(null);
		}
		
		if (reader != null) {
			reader.inject();
		}
		
	}
	
	public CommandSender getOwner() {
		return owner;
	}
	
}
