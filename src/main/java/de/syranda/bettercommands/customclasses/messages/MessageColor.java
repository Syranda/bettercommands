package de.syranda.bettercommands.customclasses.messages;

import de.syranda.bettercommands.ConfigValues;

public enum MessageColor {

	NORMAL("n"), SUB_NORMAL("sn"), SUCCESS("s"), FAILED("f"), HIGHLIGHT("h"), SUB_HIGHLIGHT("sh"), MINOR("m"), RESET("r");
	
	private String colorString;
	
	private MessageColor(String colorString) {
		this.colorString = colorString;
	}
	
	public String getColorString() {
		return ConfigValues.COLOR_CHAR + colorString;
	}
	
}
