package de.syranda.bettercommands.customclasses.messages;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import de.syranda.bettercommands.Main;
import de.syranda.bettercommands.customclasses.config.ConfigRegistry;

public class CommandConfig {
	
	public static CommandConfig get(Plugin plugin) {
		return ConfigRegistry.getConfig(plugin).getCommandConfig();
	}
	
	public static CommandConfig get(String plugin) {
		return ConfigRegistry.getConfig(plugin).getCommandConfig();
	}
	
	private static final String pattern = "^(E:)?\\$[^\\s\\|]+(\\|.+)?$";
	
	public static boolean isMessagePattern(String message) {
		return message.matches(pattern);
	}

	private FileConfiguration config;
	private File configFile;

	private HashMap<String, String> messages = new HashMap<>();
	
	public CommandConfig(Plugin plugin) {
		
		configFile = new File(JavaPlugin.getProvidingPlugin(Main.class).getDataFolder(), "//messages/" + (plugin != null ? plugin.getName() : "default") + ".yml");
		config = YamlConfiguration.loadConfiguration(configFile);
		
		if (config.contains("Messages")) {
			for (String path : config.getConfigurationSection("Messages").getKeys(true)) {
				if (config.isString("Messages." + path)) {
					messages.put(path, config.getString("Messages." + path));
					
				}
			}
		}
		
		save();
		
	}

	public String translate(String message, Object... arguments) {
		
		if (isMessagePattern(message) && message.contains("|")) {
			arguments = message.split("(?<=[^\\\\])\\|")[1].split("(?<=[^\\\\]),");
			message = getMessage(message.substring(1).split("(?<=[^\\\\])\\|")[0]);
		} else if (isMessagePattern(message)) {
			message = getMessage(message.substring(1));
		}
		
		for (int i = 0; i < arguments.length; i++) {
			message = message.contains("$" + i) ? message.replace("$" + i, arguments[i].toString()) : message;
		}
		
		return message;
		
	}
	
	private String getMessage(String path) {		
		return messages.containsKey(path) ? messages.get(path) : ":fMesage '" + path + "' doesn't exist. Please fix this mistake.";
	}
	
	public void registerDefault(String path, String message) {
		if (!messages.containsKey(path)) {
			messages.put(path, message);	
		}
		config.addDefault("Messages." + path, message);
		save();
	}

	private void save() {
		config.options().copyDefaults(true);
		try {
			config.save(configFile);
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

}
