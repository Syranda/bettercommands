package de.syranda.bettercommands.customclasses.messages;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.Bukkit;

import de.syranda.bettercommands.components.ComponentAPI;
import de.syranda.bettercommands.customclasses.config.BetterCommandConfig;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;

public interface MessageTranslator {

	default String translateColor(String message, MessageColor color, String replace) {
		return Stream.of(message.split(" ")).map(word -> word.replaceAll("(?<=[^\\\\]|\\A)(" + Pattern.quote(color.getColorString()) + ")", replace)).collect(Collectors.joining(" "));
	}

	default String translateDefaultColors(String message, BetterCommandConfig config) {

		String ret = message;

		ret = translateColor(ret, MessageColor.SUB_HIGHLIGHT, config.getSubHighlightColor().toString());
		ret = translateColor(ret, MessageColor.SUB_NORMAL, config.getSubNormalColor().toString());
		ret = translateColor(ret, MessageColor.NORMAL, config.getNormalColor().toString());
		ret = translateColor(ret, MessageColor.HIGHLIGHT, config.getHighlightColor().toString());
		ret = translateColor(ret, MessageColor.SUCCESS, config.getSuccessColor().toString());
		ret = translateColor(ret, MessageColor.FAILED, config.getFailedColor().toString());
		ret = translateColor(ret, MessageColor.MINOR, config.getMinorColor().toString());
		ret = translateColor(ret, MessageColor.RESET, ChatColor.RESET.toString());

		ret = ret.replaceAll("\\\\:", ":");
		
		return ret;

	}

	default TextComponent translateComponentPattern(String message) {
		
		String temp = message;
		TextComponent ret = new TextComponent();
		String pattern = "(?<=[^\\\\]|\\A)\\{([^};\\|]+)(?:;([^}\\|]+))?(?:\\|([^}\\\\:]+)\\\\?:(([^}]+)))?\\}";
		
		Matcher matcher = Pattern.compile(pattern).matcher(temp);
		
		if(!matcher.find())
			ret.addExtra(temp.replaceAll("\\\\\\{", "{") + " ");
		else {	
			
			int index = 1;
			
			do {
				
				BaseComponent baseComponent = translateComponent(matcher.group(1), matcher.group(2), matcher.group(3) != null ? ClickEvent.Action.valueOf(matcher.group(3)) : null, matcher.group(4));
				String[] split = temp.split(pattern);
				if(index == 1 && split.length >= index - 2)
					ret.addExtra(split[index - 1]);
				ret.addExtra(baseComponent);
				if(split.length > index)				
					ret.addExtra(split[index]);
				
				index++;
				
			} while(matcher.find());
			
		}

		return Bukkit.getBukkitVersion().startsWith("1.8") || Bukkit.getBukkitVersion().startsWith("1.9") ? new TextComponent(ret.toPlainText().substring(4)) : ret;
		
	}
	
	default BaseComponent translateComponent(String message, String hover, Action action, String value) {	
		return ComponentAPI.getComponent(message, hover, action, value);					
	}

	public TextComponent translate(boolean prefix, BetterCommandConfig config, String message, Object... arguments);

	default TextComponent translateError(BetterCommandConfig config, String message, Object... arguments) {
		return translate(config.appendPrefixOnError(), config, ":f" + message, arguments);
	}
	
	default TextComponent translate(BetterCommandConfig config, String message, Object... arguments) {
		return translate(true, config, message, arguments);
	}

}
