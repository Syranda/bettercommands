package de.syranda.bettercommands.customclasses.helper.parameters;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;

public class DoubleParameter extends CommandParameter<Double> {

	public DoubleParameter(String name, ParameterType parameterType) {
		super(name, parameterType);
	}
	
	@Override
	public ValidationPayload<Double> validate(CommandSender sender, CommandValue<Double> value, ParameterSet parameterSet) {
		ValidationPayload<Double> payload = new ValidationPayload<>();
		return value.isDouble()? payload.successWithObject(value.asDouble()) : payload.failWithMessage(":fThe " + this.getName() + " has to be a floating point number");
	}
	
}
