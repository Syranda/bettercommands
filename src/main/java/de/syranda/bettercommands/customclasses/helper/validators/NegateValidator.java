package de.syranda.bettercommands.customclasses.helper.validators;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterRegistry;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.validators.SubValidator;
import de.syranda.bettercommands.customclasses.parameter.validators.SubValidatorPayload;

public class NegateValidator extends SubValidator {

	private Class<?>[] forClasses;
	public NegateValidator(Class<?>[] forClasses) {
		this.forClasses = forClasses;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public SubValidatorPayload validate(CommandSender sender, CommandValue<?> value, ParameterSet parameterSet) {

		SubValidatorPayload payload = new SubValidatorPayload();
		
		for (Class<?> clazz : this.forClasses) {
			
			CommandParameter<?> parameter = ParameterRegistry.getParameterForType("", ParameterType.CONSTRAINT, clazz, null);
			
			if (!parameter.validate(sender, new CommandValue( value.asObject(), value.getParameter()), parameterSet).hasFailed()) {
				return payload.addVar(value.asString()).addVar(clazz.getSimpleName()).failWithMessage(parameter.getNegateErrorMessage(clazz.getSimpleName(), value));
			}
			
		}
		
		return payload.success();
	}

}
