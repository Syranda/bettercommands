package de.syranda.bettercommands.customclasses.helper.parameters;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;

public class GameModeParameter extends CommandParameter<GameMode> {

	public GameModeParameter(String name, ParameterType parameterType) {
		super(name, parameterType);
	}
	
	@Override
	public ValidationPayload<GameMode> validate(CommandSender sender, CommandValue<GameMode> value, ParameterSet parameterSet) {
		
		GameMode gm = null;
		
		if (value.isInt()) {
			switch (value.asInt()) {
			case 0:
				gm = GameMode.SURVIVAL;
				break;
			case 1:
				gm = GameMode.CREATIVE;
				break;	
			case 2:
				gm = GameMode.ADVENTURE;
				break;	
			case 3:
				gm = GameMode.SPECTATOR;
				break;	
			}
		}
		
		try {
			gm = GameMode.valueOf(value.asString());
		} catch (Exception e) {
		}
		
		return gm == null
					? new ValidationPayload<GameMode>().failWithMessage("Gamemode '" + value.asString() + "' wasn't found")
					: new ValidationPayload<GameMode>().successWithObject(gm);
	}
	
	@Override
	public List<String> getTabComplete(CommandSender sender, ParameterSet parameterSet) {
		return Stream.concat(Stream.of(GameMode.values()).map(Enum::toString), Stream.of(0, 1, 2, 3).map(number -> number + "")).collect(Collectors.toList());
	}

}
