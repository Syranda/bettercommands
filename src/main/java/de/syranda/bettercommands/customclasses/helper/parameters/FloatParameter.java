package de.syranda.bettercommands.customclasses.helper.parameters;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;

public class FloatParameter extends CommandParameter<Float> {

	public FloatParameter(String name, ParameterType parameterType) {
		super(name, parameterType);
	}

	@Override
	public ValidationPayload<Float> validate(CommandSender sender, CommandValue<Float> value, ParameterSet parameterSet) {
		ValidationPayload<Float> payload = new ValidationPayload<>();
		return value.isFloat() ? payload.successWithObject(value.asFloat()) : payload.failWithMessage(":fThe " + this.getName() + " has to be a floating point number");
	}
	
}
