package de.syranda.bettercommands.customclasses.helper.parameters;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;

public class ShortParameter extends CommandParameter<Short> {

	public ShortParameter(String name, ParameterType parameterType) {
		super(name, parameterType);
	}

	@Override
	public ValidationPayload<Short> validate(CommandSender sender, CommandValue<Short> value, ParameterSet parameterSet) { 
		ValidationPayload<Short> payload = new ValidationPayload<>();
		return value.isShort() ? payload.successWithObject(value.asShort()) : payload.failWithMessage(":fThe " + this.getName() + " has to be a short");
	}
	
}
