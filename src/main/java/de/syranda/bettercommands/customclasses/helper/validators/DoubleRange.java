package de.syranda.bettercommands.customclasses.helper.validators;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.validators.SubValidator;
import de.syranda.bettercommands.customclasses.parameter.validators.SubValidatorPayload;

@Deprecated
public class DoubleRange extends SubValidator {

	private Double min;
	private Double max;
	
	@Override
	public SubValidator fabricate(CommandValue<?>[] args) {
		this.min = args.length <= 0 || args[0].isNull() ? null : args[0].asDouble();
		this.max = args.length <= 1 || args[1].isNull() ? null : args[1].asDouble();
		return this;
	}
	
	@Override
	public SubValidatorPayload validate(CommandSender sender, CommandValue<?> value) {
		
		SubValidatorPayload payload = new SubValidatorPayload();
		
		if (!value.isDouble() && !value.isFloat()) {
			return payload.failWithMessage("The input has to be a number");
		}
		
		return (this.min != null ? this.min <= value.asDouble() : true) && (this.max != null ? this.max >= value.asDouble() : true) 
				? payload.success() 
				: payload.failWithMessage(getValidationFailedMessage(sender, value));

	}

	public String getValidationFailedMessage(CommandSender sender, CommandValue<?> value) {
		String name = value.getParameter() != null ? value.getParameter().getName() : "number";
		if (this.min != null && this.max != null) {
			return ":fThe " + name + " has to be between " + this.min + " and " + this.max;
		} else if (this.min != null && this.max == null) {
			return ":fThe " + name + " has to be greater than " + this.min;
		} else if (this.min == null && this.max != null) {
			return ":fThe " + name + " has to be lower than " + this.max;	
		} else {
			return ":fWhy are you even using this validator?";	
		}
	}
	
}
