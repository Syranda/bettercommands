package de.syranda.bettercommands.customclasses.helper.parameters;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;

public class StringParameter extends CommandParameter<String> {
	
	public StringParameter(String name, ParameterType parameterType) {
		super(name, parameterType);	
	}

	@Override
	public ValidationPayload<String> validate(CommandSender sender, CommandValue<String> value, ParameterSet parameterSet) {
		return new ValidationPayload<String>().successWithObject(value.asString());
	}
	
}
