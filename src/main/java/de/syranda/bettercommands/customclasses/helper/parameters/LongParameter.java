package de.syranda.bettercommands.customclasses.helper.parameters;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;

public class LongParameter extends CommandParameter<Long> {

	public LongParameter(String name, ParameterType parameterType) {
		super(name, parameterType);	
	}

	@Override
	public ValidationPayload<Long> validate(CommandSender sender, CommandValue<Long> value, ParameterSet parameterSet) {
		ValidationPayload<Long> payload = new ValidationPayload<>();
		return value.isLong()? payload.successWithObject(value.asLong()) : payload.failWithMessage(":fThe " + getName() + " has to be an integer");
	}
	
}
