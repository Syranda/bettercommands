package de.syranda.bettercommands.customclasses.helper.validators;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.validators.SubValidator;
import de.syranda.bettercommands.customclasses.parameter.validators.SubValidatorPayload;

public class IsLevelValidator extends SubValidator {

	private int level;
	
	@Override
	public SubValidator fabricate(CommandValue<?>[] args) {
		this.level = args[0].asInt();
		return this;
	}
	
	@Override
	public SubValidatorPayload validate(CommandSender sender, CommandValue<?> value) {
		
		SubValidatorPayload payload = new SubValidatorPayload();
		
		if (!(sender instanceof Player)) {
			return payload.success();
		}
			
		return ((Player) sender).getLevel() >= level ? payload.success() : payload.failWithMessage("You need to be level " + level + " or higher to execute this command");
		
	}

}
