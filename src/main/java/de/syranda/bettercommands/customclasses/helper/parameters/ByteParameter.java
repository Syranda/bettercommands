package de.syranda.bettercommands.customclasses.helper.parameters;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;

public class ByteParameter extends CommandParameter<Byte> {

	public ByteParameter(String name, ParameterType parameterType) {
		super(name, parameterType);
	}

	@Override
	public ValidationPayload<Byte> validate(CommandSender sender, CommandValue<Byte> value, ParameterSet parameterSet) {
		ValidationPayload<Byte> payload = new ValidationPayload<>();
		return value.isByte() ? payload.successWithObject(value.asByte()) : payload.failWithMessage(":fThe " + this.getName() + " has to be a byte");
	}
	
}
