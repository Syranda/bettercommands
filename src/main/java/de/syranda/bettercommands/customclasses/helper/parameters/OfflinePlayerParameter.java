package de.syranda.bettercommands.customclasses.helper.parameters;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;

public class OfflinePlayerParameter extends CommandParameter<OfflinePlayer> {

	public OfflinePlayerParameter(String name, ParameterType parameterType) {
		super(name, parameterType, () -> Stream.of(Bukkit.getOfflinePlayers()).map(OfflinePlayer::getName).collect(Collectors.toList()));		
	}

	@SuppressWarnings("deprecation")
	@Override
	public ValidationPayload<OfflinePlayer> validate(CommandSender sender, CommandValue<OfflinePlayer> value, ParameterSet parameterSet) {
		ValidationPayload<OfflinePlayer> payload = new ValidationPayload<>();
		OfflinePlayer oPlayer = Bukkit.getOfflinePlayer(value.asString());
		return oPlayer != null ? payload.successWithObject(oPlayer) : payload.failWithMessage(":fPlayer '" + value.asString() + "' doesn't exist");
	}
	
}
