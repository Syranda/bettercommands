package de.syranda.bettercommands.customclasses.helper.validators.annotations;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Deprecated
@Retention(RUNTIME)
@Target(PARAMETER)
public @interface DRange {
	String[] value();
}
