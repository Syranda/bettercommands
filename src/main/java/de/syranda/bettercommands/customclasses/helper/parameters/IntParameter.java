package de.syranda.bettercommands.customclasses.helper.parameters;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;

public class IntParameter extends CommandParameter<Integer> {

	public IntParameter(String name, ParameterType parameterType) {
		super(name, parameterType);	
	}

	@Override
	public ValidationPayload<Integer> validate(CommandSender sender, CommandValue<Integer> value, ParameterSet parameterSet) {
		ValidationPayload<Integer> payload = new ValidationPayload<>();
		return value.isInt() ? payload.successWithObject(value.asInt()) : payload.failWithMessage(":fThe " + this.getName() + " has to be an integer");
	}
	
}
