package de.syranda.bettercommands.customclasses.helper.parameters;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;

public class MaterialParameter extends CommandParameter<Material> {

	public MaterialParameter(String name, ParameterType parameterType) {
		super(name, parameterType, () -> Stream.of(Material.values()).map(Material::name).collect(Collectors.toList()));
	}
	
	@Override
	public ValidationPayload<Material> validate(CommandSender sender, CommandValue<Material> value, ParameterSet parameterSet) {
		ValidationPayload<Material> payload = new ValidationPayload<>();
		return value.isMaterial() ? payload.successWithObject(value.asMaterial()) : payload.failWithMessage(":fPlease enter a valid material.");
	}
	
}
