package de.syranda.bettercommands.customclasses.helper.parameters;

import java.util.stream.Collectors;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.config.BetterCommandConfig;
import de.syranda.bettercommands.customclasses.config.ConfigRegistry;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;

public class BetterCommandConfigParameter extends CommandParameter<BetterCommandConfig> {

	public BetterCommandConfigParameter(String name, ParameterType parameterType) {
		super(name, parameterType, () -> ConfigRegistry.getConfigs().stream().map(BetterCommandConfig::getPluginName).collect(Collectors.toList()));
	}

	@Override
	public ValidationPayload<BetterCommandConfig> validate(CommandSender sender, CommandValue<BetterCommandConfig> value, ParameterSet parameterSet) {
		BetterCommandConfig config = ConfigRegistry.getConfig(value.asString());
		ValidationPayload<BetterCommandConfig> payload = new ValidationPayload<>();		
		
		return config == null ? payload.failWithMessage(":fConfig of Plugin '" + value.asString() + "' doesn't exist.") : payload.successWithObject(config);		
	}
	
}
