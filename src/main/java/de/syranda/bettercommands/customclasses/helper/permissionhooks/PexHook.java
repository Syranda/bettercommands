package de.syranda.bettercommands.customclasses.helper.permissionhooks;

import java.util.Collection;

import de.syranda.bettercommands.customclasses.permissionhooks.PermissionHook;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class PexHook implements PermissionHook {

	public Collection<String> getGroups() {
		return PermissionsEx.getPermissionManager().getGroupNames();
	}

	public boolean canExecute(String group, String permission) {
		return PermissionsEx.getPermissionManager().getGroup(group).has(permission);
	}

	public void addGroupPermission(String group, String permission) {
		PermissionsEx.getPermissionManager().getGroup(group).addPermission(permission);		
	}

	public void removeGroupPermission(String group, String permission) {
		PermissionsEx.getPermissionManager().getGroup(group).removePermission(permission);			
	}

	public void addPermission(String player, String permission) {
		PermissionsEx.getUser(player).addPermission(permission);		
	}

	public void removePermission(String player, String permission) {
		PermissionsEx.getUser(player).removePermission(permission);			
	}
	
}
