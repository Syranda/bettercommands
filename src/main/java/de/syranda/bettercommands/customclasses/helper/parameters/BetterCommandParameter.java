package de.syranda.bettercommands.customclasses.helper.parameters;

import java.util.stream.Collectors;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.BetterCommand;
import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;

public class BetterCommandParameter extends CommandParameter<BetterCommand> {

	public BetterCommandParameter(String name, ParameterType parameterType) {
		super(name, parameterType, () -> BetterCommand.getBetterCommands().stream().map(BetterCommand::getName).collect(Collectors.toList()));
	}

	@Override
	public ValidationPayload<BetterCommand> validate(CommandSender sender, CommandValue<BetterCommand> value, ParameterSet parameterSet) {
		BetterCommand command = BetterCommand.getBetterCommand(value.asString());
		ValidationPayload<BetterCommand> payload = new ValidationPayload<>();		
		
		return command == null ? payload.failWithMessage(":fConfig of Plugin '" + value.asString() + "' doesn't exist.") : payload.successWithObject(command);	
	}
	
}
