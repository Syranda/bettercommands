package de.syranda.bettercommands.customclasses.helper.parameters;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;

public class PluginParameter extends CommandParameter<Plugin> {

	public PluginParameter(String name, ParameterType parameterType) {
		super(name, parameterType, () -> Stream.of(Bukkit.getPluginManager().getPlugins()).map(plugin -> plugin.getName()).collect(Collectors.toList()) /* 1. */);
	}

	@Override
	public ValidationPayload<Plugin> validate(CommandSender sender, CommandValue<Plugin> value, ParameterSet parameterSet) {
		ValidationPayload<Plugin> payload = new ValidationPayload<>();
		Plugin plugin = Bukkit.getPluginManager().getPlugin(value.asString());
		return plugin != null ? payload.successWithObject(plugin) : payload.failWithMessage(":fPlugin " + value.asString() + " does not exist.");
	}
	
}
