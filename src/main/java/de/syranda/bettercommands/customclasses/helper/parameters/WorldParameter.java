package de.syranda.bettercommands.customclasses.helper.parameters;

import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;

public class WorldParameter extends CommandParameter<World> {

	public WorldParameter(String name, ParameterType parameterType) {
		super(name, parameterType, () -> Bukkit.getWorlds().stream().map(World::getName).collect(Collectors.toList()));
	}

	@Override
	public ValidationPayload<World> validate(CommandSender sender, CommandValue<World> value, ParameterSet parameterSet) {
		ValidationPayload<World> payload = new ValidationPayload<>();
		World world = Bukkit.getWorld(value.asString());
		return world != null ? payload.successWithObject(world) : payload.failWithMessage(":fThe world " + value.asString() + " was not found");
	}
	
}
