package de.syranda.bettercommands.customclasses.helper.parameters;

import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;

public class PlayerParameter extends CommandParameter<Player> {
	
	public PlayerParameter(String name, ParameterType parameterType) {
		super(name, parameterType, () -> Bukkit.getOnlinePlayers().stream().map(Player::getName).collect(Collectors.toList()));
	}
	
	@Override
	public CommandValue<Player> getDefaultValue(CommandSender sender, CommandValue<Player> value) {
		return sender instanceof Player && value.asString().equalsIgnoreCase("self") ? 
				new CommandValue<Player>(((Player) sender).getName(), this) : new CommandValue<Player>(null);
	}

	@Override
	public ValidationPayload<Player> validate(CommandSender sender, CommandValue<Player> value, ParameterSet parameterSet) {
		ValidationPayload<Player> payload = new ValidationPayload<>();
		Player player = Bukkit.getPlayer(value.asString());
		return player != null ? payload.successWithObject(player) : payload.failWithMessage(":fPlayer '" + value.asString() + "' is not online");
	}
	
}
