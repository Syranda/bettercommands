package de.syranda.bettercommands.customclasses.helper.parameters;

import java.util.Arrays;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;

public class BooleanParameter extends CommandParameter<Boolean> {

	public BooleanParameter(String name, ParameterType parameterType) {
		super(name, parameterType, () -> Arrays.asList("true", "false"));
	}

	@Override
	public ValidationPayload<Boolean> validate(CommandSender sender, CommandValue<Boolean> value, ParameterSet parameterSet) {
		ValidationPayload<Boolean> payload = new ValidationPayload<>();		
		return value.isBoolean()? payload.successWithObject(value.asBoolean()) : payload.failWithMessage(":fThe " + this.getName() + " has to be a boolean");
	}
	
	@Override
	public CommandValue<Boolean> getDefaultValue(CommandSender sender, CommandValue<Boolean> value) {
		if (value.isNull() && this.getParameterType().isOptional()) {
			return new CommandValue<>(false, this);
		} else {
			return value;
		}
	}
	
}
