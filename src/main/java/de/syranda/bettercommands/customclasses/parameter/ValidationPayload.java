package de.syranda.bettercommands.customclasses.parameter;

import java.util.ArrayList;
import java.util.List;

import de.syranda.bettercommands.customclasses.Payload;

public class ValidationPayload<N> extends Payload {
	
	private N validatedObject;

	public ValidationPayload() {
		this.set("success", false);
		this.set("vars", new ArrayList<>());
	}
	
	public N getValidatedObject() {
		return validatedObject;
	}
	
	public boolean hasFailed() {
		return this.get("success").equals(false);
	}
	
	public ValidationPayload<N> failWithMessage(String message) {
		this.validatedObject = null;
		this.set("errorMessage", message);
		this.set("success", false);
		return this;
	}
	
	public ValidationPayload<N> fail() {
		this.validatedObject = null;
		this.set("success", false);
		return this;
	}
	
	public String getErrorMessage() {
		return (String) this.get("errorMessage");
	}
	
	public ValidationPayload<N> successWithObject(N validatedObject) {
		this.validatedObject = validatedObject;
		this.set("success", true);
		return this;
	}
	@SuppressWarnings("unchecked")
	public List<String> getVars() {
		return (List<String>) this.get("vars");
	}
	@SuppressWarnings("unchecked")
	public ValidationPayload<N> addVar(String var) {
		((List<String>) this.get("vars")).add(var);
		return this;
	}
	
}
