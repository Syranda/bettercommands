package de.syranda.bettercommands.customclasses.parameter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.SubCommand;

@SuppressWarnings("rawtypes")
public class ParameterSet {

	private SubCommand subCommand;
	private CommandValue[] parameters;
	
	public ParameterSet(SubCommand subCommand, CommandValue[] parameters) {
		this.subCommand = subCommand;
		this.parameters = parameters;		
	}
	
	public CommandValue[] getParameters() {
		return parameters;
	}
	
	public CommandValue get(String name) {	
		int index = indexOf(name);
		return index >= 0 && index < parameters.length ? parameters.clone()[index] : new CommandValue<>(null);
	}
	
	@SuppressWarnings("unchecked")
	public <N> List<CommandValue<N>> get(Class<? extends CommandParameter<N>> parameter) {
		return Stream.of(parameters).filter(value -> value.getParameter() != null && value.getParameter().getClass().equals(parameter)).map(value -> {
			
			if (value.getParameter().getParameterType().isArray()) {
				return new CommandValue<N>(String.join(" ", Stream.of(parameters)
																.skip(indexOf(value.getParameter().getName()))
																.map(CommandValue::asString)
																.collect(Collectors.toList())), value.getParameter());				
			} else {				
				return value;				
			}
			
		}).collect(Collectors.toList());
	}

	@SuppressWarnings("unchecked")
	public <N> List<N> getByType(Class<N> parameter) {
		return (List<N>) Stream.of(parameters).filter(value -> parameter.isAssignableFrom(value.asValidatedObject().getClass())).map(value -> value.asValidatedObject()).collect(Collectors.toList());
	}
	
	public CommandValue[] getArray(String name) {		
		return Arrays.copyOfRange(parameters, indexOf(name), parameters.length);		
	}
	
	public boolean isSet(String name) {		
		return Stream.of(parameters).anyMatch(p -> p.getParameter().getName().equals(name));	
	}
	
	public <T extends Enum<T>> boolean isEnum(String name, Class<T> enu) {		
		try {
			Enum.valueOf(enu, get(name).asString());
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}		
	}
	
	private int indexOf(String name) {	
		for (int i = 0; i < subCommand.getParameters().size(); i++) {
			if (subCommand.getParameters().get(i).getName().equalsIgnoreCase(name)) {
				return i;
			}
		}			
		return -1;		
	}		
	
	public void addNull(CommandParameter<?> parameter) {
		this.parameters = Arrays.copyOf(this.parameters, this.parameters.length + 1);
		this.parameters[this.parameters.length - 1] = new CommandValue<>(null, parameter);
	}

	@SuppressWarnings("unchecked")
	public void setDefault(CommandSender sender, CommandParameter<?> parameter, String value) {
		CommandValue<?> add = parameter.getDefaultValue(sender, new CommandValue(value, parameter));
		add.validate(sender, new ParameterSet(this.subCommand, this.parameters));
		this.parameters = Arrays.copyOf(this.parameters, this.parameters.length + 1);		
		int index = this.indexOf(parameter.getName());
 		this.parameters[index] = add;
	}
	
	@Override
	public String toString() {
		return "ParameterSet [" + Stream.of(this.getParameters()).map(value -> value.toString()).collect(Collectors.joining(", ")) + "]";
	}
	
}
