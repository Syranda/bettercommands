package de.syranda.bettercommands.customclasses.parameter.validators;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.helper.validators.DoubleRange;
import de.syranda.bettercommands.customclasses.helper.validators.IsLevelValidator;
import de.syranda.bettercommands.customclasses.helper.validators.NumberRange;
import de.syranda.bettercommands.customclasses.helper.validators.annotations.DRange;
import de.syranda.bettercommands.customclasses.helper.validators.annotations.Level;
import de.syranda.bettercommands.customclasses.helper.validators.annotations.Range;

public final class ValidatorRegistry {

	private ValidatorRegistry() {
	}
	
	private static HashMap<Class<?>, Class<? extends SubValidator>> registry = new HashMap<>();

	static {
		registerCommandValidator(Level.class, IsLevelValidator.class);
		registerCommandValidator(Range.class, NumberRange.class);
		registerCommandValidator(DRange.class, DoubleRange.class);
	}
	
	public static void registerCommandValidator(Class<?> annotation, Class<? extends SubValidator> validator) {	
		registry.put(annotation, validator);
	}
	
	@SuppressWarnings("rawtypes")
	public static SubValidator getValidator(Annotation annotation) {
		
		if (!registry.containsKey(annotation.annotationType())) {
			return null;
		}
		
		try {
		
			Method argMethod = Stream.of(annotation.getClass().getDeclaredMethods()).filter(method -> method.getName().equals("value")).findAny().orElse(null);
			CommandValue<?>[] values = null;
			if (argMethod != null) {
				Method getValues = annotation.getClass().getDeclaredMethod("value");
				getValues.setAccessible(true);
				String[] arguments = (String[]) getValues.invoke(annotation);
				values = Stream.of(arguments).map(arg -> new CommandValue(arg)).collect(Collectors.toList()).toArray(new CommandValue[arguments.length]);
			}
			return ((SubValidator) registry.get(annotation.annotationType()).newInstance()).fabricate(values);
			
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
}
