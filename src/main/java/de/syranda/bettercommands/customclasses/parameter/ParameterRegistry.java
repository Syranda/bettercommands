package de.syranda.bettercommands.customclasses.parameter;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

import de.syranda.bettercommands.customclasses.CommandValue;

public final class ParameterRegistry {

	private ParameterRegistry() {
	}
	
	private static HashMap<Class<?>, Class<? extends CommandParameter<?>>> registry = new HashMap<>();
	
	public static HashMap<Class<?>, Class<? extends CommandParameter<?>>> getRegistry() {
		return registry;
	}
	
	public static CommandParameter<?> getParameterForType(String parameterName, ParameterType parameterType, Class<?> clazz, CommandValue<?>[] values) {
		
		Class<? extends CommandParameter<?>> parameterClass = registry.get(clazz);
		
		try {
			if (parameterClass != null) {
				CommandParameter<?> ret = parameterClass.getConstructor(String.class, ParameterType.class).newInstance(parameterName, parameterType).fabricate(values);
				if (ret instanceof AnnotationParameter) {
					((AnnotationParameter) ret).setAnnotationMethods(AnnotationParameterMethods.getForParameter(clazz));
				} else if (ret instanceof SimpleParameter) {
					((SimpleParameter) ret).setSimpleMethods(SimpleParameterMethods.getForParameter(clazz));
				}
				return ret;
			}
			return null;
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void registerParameter(Class<?> forClass, Class<? extends CommandParameter<?>> parameter) {
		registry.put(forClass, parameter);
	}

	public static Class<?> getTypeForParameter(Class<? extends CommandParameter<?>> parameter) {
		return registry.entrySet().stream().filter(entry -> entry.getValue().equals(parameter)).findAny().orElse(null).getKey();
	}
 	
	public static <N> void registerSimpleParameter(Class<N> clazz, List<N> content, Function<N, String> function) {
		registerSimpleParameter(clazz, clazz.getSimpleName(), content, function);
	}
	
	public static <N> void registerSimpleParameter(Class<N> clazz, String name, List<N> content, Function<N, String> function) {
		registerSimpleParameter(clazz, content, function, name + " '$0' doesn't exist", name + " '$0' does already exist");
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <N> void registerSimpleParameter(Class<N> clazz, List<N> content, Function<N, String> function, String errorMessage, String duplicateError) {
		SimpleParameter parameter = new SimpleParameter(null, null);
		SimpleParameterMethods methods = new SimpleParameterMethods<>(clazz, content, function, errorMessage, duplicateError);
		SimpleParameterMethods.addMethods(methods);
		registerParameter(clazz, (Class<? extends CommandParameter<?>>) parameter.getClass());
	}
	
}
