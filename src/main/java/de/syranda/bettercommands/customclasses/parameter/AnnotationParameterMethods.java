package de.syranda.bettercommands.customclasses.parameter;

import java.lang.reflect.Method;
import java.util.HashMap;

public class AnnotationParameterMethods {

	private static HashMap<Class<?>, AnnotationParameterMethods> methods = new HashMap<>();
	
	public static AnnotationParameterMethods getForParameter(Class<?> clazz) {
		return methods.get(clazz);
	}
	
	public static void setMethods(Class<?> forClass, AnnotationParameterMethods methods) {
		AnnotationParameterMethods.methods.put(forClass, methods);
	}
	
	private final Object invokationObject;
	private final Method validate;
	private final Method tabComplete;
	private final String errorMessage;
	
	public AnnotationParameterMethods(Object invokationObject, Method validate, Method tabComplete, String errorMessage) {
		this.invokationObject = invokationObject;
		this.validate = validate;
		this.tabComplete = tabComplete;
		this.errorMessage = errorMessage;
	}

	public final Object getInvokationObject() {
		return this.invokationObject;
	}
	
	public final Method getValidate() {
		return this.validate;
	}
	
	public final Method getTabComplete() {
		return this.tabComplete;
	}
	
	public final String getErrorMessage() {
		return this.errorMessage;
	}

}
