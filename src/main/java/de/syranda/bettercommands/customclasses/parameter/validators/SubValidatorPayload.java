package de.syranda.bettercommands.customclasses.parameter.validators;

import java.util.ArrayList;
import java.util.List;

import de.syranda.bettercommands.customclasses.Payload;

public class SubValidatorPayload extends Payload {

	private boolean success = false;

	public SubValidatorPayload() {
		this.set("vars", new ArrayList<>());
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	public SubValidatorPayload success() {
		this.success = true;
		return this;
	}
	
	public SubValidatorPayload failWithMessage(String message) {
		this.set("errorMessage", message);
		return this;
	}
	
	public String getErrorMessage() {
		return this.get("errorMessage").toString();
	}	
	@SuppressWarnings("unchecked")
	public List<String> getVars() {
		return (List<String>) this.get("vars");
	}
	@SuppressWarnings("unchecked")
	public SubValidatorPayload addVar(String var) {
		((List<String>) this.get("vars")).add(var);
		return this;
	}
	
}
