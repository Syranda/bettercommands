package de.syranda.bettercommands.customclasses.parameter;

public enum ParameterType {

	CONSTRAINT(false, false), OPTIONAL(true, false), CONSTRAINT_ARRAY(false, true), OPTIONAL_ARRAY(true, true);
	
	private boolean optional;
	private boolean array;
	
	private ParameterType(boolean optional, boolean array) {
		this.optional = optional;
		this.array = array;
	}
	
	public boolean isOptional() {
		return optional;
	}
	
	public boolean isArray() {
		return array;
	}
	
}
