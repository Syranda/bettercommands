package de.syranda.bettercommands.customclasses.parameter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import org.apache.commons.lang.WordUtils;
import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.ConfigValues;
import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.validators.SubValidator;
import de.syranda.bettercommands.customclasses.parameter.validators.SubValidatorPayload;

public abstract class CommandParameter<N> {
	
	private String name;
	private ParameterType parameterType;
	private Callable<List<String>> callable;
	private List<SubValidator> subValidators;
	
	public CommandParameter(String name, ParameterType parameterType) {
		this(name, parameterType, null);
	}
	
	protected CommandParameter(String name, ParameterType parameterType, Callable<List<String>> callable) {
		this.name = name;
		this.parameterType = parameterType;
		this.callable = callable;
		this.subValidators = new ArrayList<>();
	}
	
	public final String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public final ParameterType getParameterType() {
		return parameterType;
	}
	
	public final void setParameterType(ParameterType parameterType) {
		this.parameterType = parameterType;
	}

	public abstract ValidationPayload<N> validate(CommandSender sender, CommandValue<N> value, ParameterSet parameterSet);
		
	public final List<String> getTabComplete() {
		try {
			return callable.call();
		} catch (Exception e) {
			return new ArrayList<String>();
		}
	}
	
	public List<String> getTabComplete(CommandSender sender, ParameterSet parameterSet) {
		return this.getTabComplete();
	}
		
	public final List<String> getInternalTabComplete(CommandSender sender, String current, ParameterSet parameterSet) {
		return this.getTabComplete(sender, parameterSet).stream()
				.filter(line -> line.startsWith(current))
				.filter(value -> !new CommandValue<>(value, this).validate(sender, parameterSet).hasFailed())
				.collect(Collectors.toList());
	}
	
	public String getConversationMessage() {
		return ":nPlease enter a :h" + WordUtils.capitalize(name) + ":n:";
	}
		
	public String getOptionalConversationMessage() {
		return getConversationMessage() + " :m(Optional; '" + ConfigValues.OPTIONAL_NONE + "' for no value)";
	}
	
	public CommandParameter<N> fabricate(CommandValue<?>[] values) {
		return this;
	}

	public final ValidationPayload<N> internalValidate(CommandSender sender, CommandValue<N> value, ParameterSet parameterSet) {
		try {
			return validate(sender, value, parameterSet);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public final List<SubValidatorPayload> getFailedSubValidators(CommandSender sender, CommandValue<N> value, ParameterSet parameterSet) {
		return this.subValidators.stream().map(val -> val.validate(sender, value, parameterSet)).filter(payload -> !payload.isSuccess()).collect(Collectors.toList());
	}
	
	public final void addSubValidator(SubValidator subValidator) {
		this.subValidators.add(subValidator);
	}
	
	public CommandValue<N> getDefaultValue(CommandSender sender, CommandValue<N> value) {
		return value;
	}
	
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " [name: " + getName() + "]";
	}
	
	public String getNegateErrorMessage(String type, CommandValue<?> value) {
		return "Error: '$0' is a $1";
	}
	
}
