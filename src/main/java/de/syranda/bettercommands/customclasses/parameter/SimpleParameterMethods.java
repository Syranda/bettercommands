package de.syranda.bettercommands.customclasses.parameter;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@SuppressWarnings("rawtypes")
public class SimpleParameterMethods<N> {

	private static List<SimpleParameterMethods> methods = new ArrayList<>();
	
	public static SimpleParameterMethods getForParameter(Class<?> clazz) {
		return methods.stream().filter(parameter -> parameter.getOwner().equals(clazz)).findAny().orElse(null);
	}
	
	public static void addMethods(SimpleParameterMethods methods) {
		SimpleParameterMethods.methods.add(methods);
	}
	
	private final Class<N> owner;
	private final List<N> content;
	private final Function<N, String> function;
	private final String errorMessage;
	private final String duplicateErrorMessage;
	
	public SimpleParameterMethods(Class<N> owner, List<N> content, Function<N, String> function, String errorMessage, String duplicateErrorMessage) {
		this.owner = owner;
		this.content = content;
		this.function = function;
		this.errorMessage = errorMessage;
		this.duplicateErrorMessage = duplicateErrorMessage;
	}

	public Class<N> getOwner() {
		return owner;
	}

	public List<N> getContent() {
		return content;
	}

	public Function<N, String> getFunction() {
		return function;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public String getDuplicateErrorMessage() {
		return duplicateErrorMessage;
	}
	
}
