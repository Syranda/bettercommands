package de.syranda.bettercommands.customclasses.parameter.validators;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;

public abstract class SubValidator {
	
	@Deprecated
	public SubValidatorPayload validate(CommandSender sender, CommandValue<?> value) {
		return new SubValidatorPayload().success();
	}
	
	public SubValidatorPayload validate(CommandSender sender, CommandValue<?> value, ParameterSet parameterSet) {
		return this.validate(sender, value);
	}

	public SubValidator fabricate(CommandValue<?>[] args) {
		return this;
	}
	
}
