package de.syranda.bettercommands.customclasses.parameter;

import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;

@SuppressWarnings("rawtypes")
public class SimpleParameter extends CommandParameter {

	private SimpleParameterMethods simpleMethods;
	
	public SimpleParameter(String name, ParameterType parameterType) {
		super(name, parameterType);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ValidationPayload validate(CommandSender sender, CommandValue value, ParameterSet parameterSet) {

		ValidationPayload payload = new ValidationPayload<>();
		
		Object ret = this.simpleMethods.getContent().stream().filter(entry -> this.simpleMethods.getFunction().apply(entry).equals(value.asString())).findAny().orElse(null);
		
		return ret == null
				? payload.addVar(value.asString()).failWithMessage(this.simpleMethods.getErrorMessage())
				: payload.successWithObject(ret);
				
	}
	
	public void setSimpleMethods(SimpleParameterMethods simpleMethods) {
		this.simpleMethods = simpleMethods;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List getTabComplete(CommandSender sender, ParameterSet parameterSet) {
		return (List) this.simpleMethods.getContent().stream().map(entry -> this.simpleMethods.getFunction().apply(entry)).collect(Collectors.toList());
	}
	
	@Override
	public String getNegateErrorMessage(String type, CommandValue value) {
		return this.simpleMethods.getDuplicateErrorMessage();
	}
	
}
