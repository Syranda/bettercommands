package de.syranda.bettercommands.customclasses.parameter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.CommandValue;

@SuppressWarnings("rawtypes")
public class AnnotationParameter extends CommandParameter {

	private AnnotationParameterMethods annotationMethods;
	
	public AnnotationParameter(String name, ParameterType parameterType) {
		super(name, parameterType);
	}
	
	public void setAnnotationMethods(AnnotationParameterMethods annotationMethods) {
		this.annotationMethods = annotationMethods;
	}
	
	@SuppressWarnings({ "unchecked" })
	@Override
	public ValidationPayload validate(CommandSender sender, CommandValue value, ParameterSet parameterSet) {
		ValidationPayload ret = new ValidationPayload<>();		
		try {
			Object obj = this.annotationMethods.getValidate().invoke(this.annotationMethods.getInvokationObject(), this.getParamters(this.annotationMethods.getValidate(), sender, value, parameterSet));
			
			if (this.annotationMethods.getValidate().getReturnType() == ValidationPayload.class) {
				return (ValidationPayload) obj;
			} else if (obj instanceof String) {
				return ret.failWithMessage((String) obj);
			} else if (obj == null) {
				return ret.failWithMessage((String) this.annotationMethods.getErrorMessage()).addVar(value.asString());
			} else {
				return ret.successWithObject(obj);
			}
 			
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			return ret.fail();
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getTabComplete(CommandSender sender, ParameterSet parameterSet) {
		
		Method tabMethod = this.annotationMethods.getTabComplete();
		if (tabMethod != null) {
			try {
				return (List<String>) tabMethod.invoke(this.annotationMethods.getInvokationObject(), this.getParamters(this.annotationMethods.getTabComplete(), sender, null, parameterSet));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return new ArrayList<>();
	}
	
	private Object[] getParamters(Method method, CommandSender sender, CommandValue value, ParameterSet parameterSet) {
		Object[] parameters = new Object[method.getParameterCount()];
		parameters[0] = sender;
		if (value != null) {
			parameters[1] = value;
		}
		
//		for (int i = 1; i < parameters.length; i++) {
//			Parameter parameter = this.inlineMethods.getValidate().getParameters()[i];
//			if (parameterSet.isSet(parameter.getName()) && parameterSet.get(parameter.getName()).asValidatedObject().getClass() == parameter.getType()) {
//				parameters[i] = parameterSet.get(parameter.getName()).asValidatedObject();
//			}
//		}
				
		HashMap<Class<?>, Integer> offsets = new HashMap<>();
		
		for (int i = value == null ? 1 : 2; i < parameters.length; i++) {
			if (parameters[i] != null) {
				continue;
			}
			Parameter parameter = method.getParameters()[i];
			if (!offsets.containsKey(parameter.getType())) {
				offsets.put(parameter.getType(), 0);
			}
			int offset = offsets.get(parameter.getType());
			List<?> list = parameterSet.getByType(parameter.getType());
			if (offset >= list.size()) {
				throw new RuntimeException("Requested parameter '" + parameter.getName() + "' is not provided in the executed command");
			}
			parameters[i] = list.get(offset);
			offsets.put(parameter.getType(), offset + 1);
		}
		return parameters;
	}
	
}
