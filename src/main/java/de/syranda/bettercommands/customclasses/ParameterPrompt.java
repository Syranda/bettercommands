package de.syranda.bettercommands.customclasses;

import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;

public class ParameterPrompt implements Prompt {
	
	@Override
	public String getPromptText(ConversationContext context) {
		return "Null";
	}

	@Override
	public boolean blocksForInput(ConversationContext context) {
		return true;		
	}

	@Override
	public Prompt acceptInput(ConversationContext context, String input) {
		return null;				
	}
	
}
