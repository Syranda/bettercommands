package de.syranda.bettercommands.customclasses;

import java.util.HashMap;

public class Payload {
	
	private HashMap<String, Object> values = new HashMap<>();
	
	public void set(String key, Object value) {
		this.values.put(key, value);
	}

	public HashMap<String, Object> getValues() {
		return this.values;
	}
	
	public Object get(String key) {
		return this.values.get(key);
	}
	
}
