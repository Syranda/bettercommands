package de.syranda.bettercommands.customclasses.nativeoverride;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import de.syranda.bettercommands.VersionManagement;
import de.syranda.bettercommands.customclasses.SubCommand;
import io.netty.channel.ChannelPipeline;

public class CommandPacketReader {

	private static List<CommandPacketReader> injectedReaders = new ArrayList<>();
	
	public static CommandPacketReader getInjectedReader(Player player) {
		return injectedReaders.stream().filter(reader -> reader.getPlayer() == player).findAny().orElse(null);
	}
	
	public static boolean hasInjectedReader(Player player) {
		return getInjectedReader(player) != null;
	}
	
	private Player player;
	private ChannelPipeline pipeline;
	private SubCommand subCommand;
	private List<Player> additionalPlayers = new ArrayList<>();

	public CommandPacketReader(Player player, SubCommand subCommand) {

		this.player = player;
		if (player != null) {
			this.pipeline = VersionManagement.getCompatiblePipeline(player);	
		}
		this.subCommand = subCommand;

	}

	public void inject() {
		
		if (pipeline != null && !pipeline.names().contains("bettercommandsreader")) {
			pipeline.addLast("bettercommandsreader", VersionManagement.getCompatibleMessageEncoder(subCommand));
		}
		
		additionalPlayers.forEach(pl -> {
			if (player != pl) {
				new CommandPacketReader(pl, subCommand).inject();
			}
		});
		
		injectedReaders.add(this);

	}

	public void remove() {

		if (pipeline != null && pipeline.names().contains("bettercommandsreader")) {
			pipeline.remove("bettercommandsreader");
		}

		additionalPlayers.forEach(player -> new CommandPacketReader(player, subCommand).remove());
		
		injectedReaders.remove(this);
		
	}

	public Player getPlayer() {
		return player;
	}
	
	public SubCommand getSubCommand() {
		return subCommand;
	}
	
	public List<Player> getAdditionalPlayers() {
		return additionalPlayers;
	}
	
}
