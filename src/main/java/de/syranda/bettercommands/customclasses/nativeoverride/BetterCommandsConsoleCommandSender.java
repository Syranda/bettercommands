package de.syranda.bettercommands.customclasses.nativeoverride;

import java.util.Set;
import java.util.stream.Stream;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;

import de.syranda.bettercommands.customclasses.SubCommand;

public class BetterCommandsConsoleCommandSender implements CommandSender {
	
	private static SubCommand currentSubCommand;
	private static CommandPacketReader reader;
	
	public static void setCurrentSubCommand(SubCommand currentSubCommand) {
		BetterCommandsConsoleCommandSender.currentSubCommand = currentSubCommand;
	}
	
	public static CommandPacketReader getReader() {
		return reader;
	}
	
	public static void setReader(CommandPacketReader reader) {
		BetterCommandsConsoleCommandSender.reader = reader;
	}
	
	public static SubCommand getCurrentSubCommand() {
		return currentSubCommand;
	}
	
	@Override
	public void sendMessage(String message) {
		
		if(hasCurrentSubCommand())
			message = currentSubCommand.getTranslator().translate(!message.startsWith("|"), currentSubCommand, message.startsWith("|") ? message.substring(1) : message).toLegacyText();
					
		Bukkit.getConsoleSender().sendMessage(message);
			
	}
		
	public boolean hasCurrentSubCommand() {
		return getCurrentSubCommand() != null;
	}

	@Override
	public PermissionAttachment addAttachment(Plugin arg0) {
		return null;
	}

	@Override
	public PermissionAttachment addAttachment(Plugin arg0, int arg1) {
		return null;
	}

	@Override
	public PermissionAttachment addAttachment(Plugin arg0, String arg1, boolean arg2) {
		return null;
	}

	@Override
	public PermissionAttachment addAttachment(Plugin arg0, String arg1, boolean arg2, int arg3) {
		return null;
	}

	@Override
	public Set<PermissionAttachmentInfo> getEffectivePermissions() {
		return null;
	}

	@Override
	public boolean hasPermission(String arg0) {
		return true;
	}

	@Override
	public boolean hasPermission(Permission arg0) {
		return true;
	}

	@Override
	public boolean isPermissionSet(String arg0) {
		return false;
	}

	@Override
	public boolean isPermissionSet(Permission arg0) {
		return false;
	}

	@Override
	public void recalculatePermissions() {		
	}

	@Override
	public void removeAttachment(PermissionAttachment arg0) {
	}

	@Override
	public boolean isOp() {
		return true;
	}

	@Override
	public void setOp(boolean arg0) {		
	}

	@Override
	public String getName() {
		return "CONSOLE";
	}

	@Override
	public Server getServer() {
		return Bukkit.getServer();
	}

	@Override
	public void sendMessage(String[] arg0) {
		Stream.of(arg0).forEach(x -> sendMessage(x));
	}
		
}
