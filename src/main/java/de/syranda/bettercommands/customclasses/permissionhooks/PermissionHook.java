package de.syranda.bettercommands.customclasses.permissionhooks;

import java.util.Collection;

public interface PermissionHook {
	
	public Collection<String> getGroups();
	
	public boolean canExecute(String group, String permission);
	
	public void addGroupPermission(String group, String permission);
	
	public void removeGroupPermission(String group, String permission);

	public void addPermission(String player, String permission);
	
	public void removePermission(String player, String permission);
	
}
