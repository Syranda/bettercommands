package de.syranda.bettercommands.customclasses.permissionhooks;

import java.util.ArrayList;
import java.util.List;

public class HookRegistry {

	private static List<Class<? extends PermissionHook>> hooks = new ArrayList<>();
	
	public static List<Class<? extends PermissionHook>> getHooks() {
		return hooks;
	}
	
	private static String usedHook;
	
	public static void setUsedHook(String usedHook) {
		HookRegistry.usedHook = usedHook;
	}
	
	public static void registerHook(Class<? extends PermissionHook> clazz) {
		hooks.add(clazz);
	}
	
	public static PermissionHook getUsedHookInstance() {
		try {
			return hooks.stream().filter(hook -> hook.getSimpleName().equals(usedHook)).findAny().orElse(null).newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			return null;
		}
	}
	
	public static String getUsedHook() {
		return usedHook;
	}
	
}
