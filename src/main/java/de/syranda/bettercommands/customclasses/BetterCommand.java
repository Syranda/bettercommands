package de.syranda.bettercommands.customclasses;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;
import org.bukkit.help.HelpTopic;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import de.syranda.bettercommands.Main;
import de.syranda.bettercommands.VersionManagement;
import de.syranda.bettercommands.customclasses.adc.Command;
import de.syranda.bettercommands.customclasses.adc.CommandLoader;
import de.syranda.bettercommands.customclasses.adc.simple.Content;
import de.syranda.bettercommands.customclasses.adc.simple.Identify;
import de.syranda.bettercommands.customclasses.adc.simple.SimpleParameter;
import de.syranda.bettercommands.customclasses.adc.simple.TabCompleter;
import de.syranda.bettercommands.customclasses.adc.simple.Validator;
import de.syranda.bettercommands.customclasses.config.BetterCommandConfig;
import de.syranda.bettercommands.customclasses.config.ConfigRegistry;
import de.syranda.bettercommands.customclasses.config.DefaultConfig;
import de.syranda.bettercommands.customclasses.messages.MessageTranslator;
import de.syranda.bettercommands.customclasses.messages.TranslatorDisabler;
import de.syranda.bettercommands.customclasses.nativeoverride.BetterCommandsConsoleCommandSender;
import de.syranda.bettercommands.customclasses.nativeoverride.CommandPacketReader;
import de.syranda.bettercommands.customclasses.parameter.AnnotationParameter;
import de.syranda.bettercommands.customclasses.parameter.AnnotationParameterMethods;
import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterRegistry;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ParameterType;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;
import de.syranda.bettercommands.customclasses.parameter.validators.SubValidator;
import de.syranda.bettercommands.customclasses.parameter.validators.ValidatorRegistry;
import de.syranda.bettercommands.customclasses.permissionhooks.HookRegistry;
import de.syranda.bettercommands.customclasses.permissionhooks.PermissionHook;
import net.md_5.bungee.api.ChatColor;

public class BetterCommand extends BukkitCommand implements BetterCommandConfig {
	
	private static ArrayList<BetterCommand> betterCommands = new ArrayList<BetterCommand>();
	
	public static BetterCommand getBetterCommand(String command) {
		return betterCommands.stream().filter(cmd -> cmd.getCommand().equals(command) || cmd.getAliases().contains(command)).findAny().orElse(null);
	}
	
	public static ArrayList<BetterCommand> getBetterCommands() {
		return betterCommands;
	}
	
	private static List<String> excludedPackages = new ArrayList<>();
	
	public static void excludePackage(String packageName) {
		excludedPackages.add(packageName);
	}
	
	private static List<TranslatorDisabler> disablers = new ArrayList<>();
	
	public static void disableTranslator(CommandSender sender) {

		CommandPacketReader reader;
		
		if (sender instanceof Player && CommandPacketReader.hasInjectedReader((Player) sender)) {
			reader = CommandPacketReader.getInjectedReader((Player) sender);
		} else if (sender instanceof BetterCommandsConsoleCommandSender) {
			reader = BetterCommandsConsoleCommandSender.getReader();
		} else {
			return;
		}
		
		TranslatorDisabler disabler = new TranslatorDisabler(sender, reader);
		disabler.disable();
		
	}
	
	public static void restoreTranslator(CommandSender sender) {
		
		TranslatorDisabler disabler = disablers.stream().filter(d -> d.getOwner() == sender).findAny().orElse(null);
		
		if (disabler == null) {
			return;
		}
		
		disabler.restore();
		
	}
	
	private static String className; // Because lambda :(
	private static List<String> loaded = new ArrayList<>();
	
	public static ContextManager scan(Plugin plugin) {
		return scan(plugin, null);
	}
	
	@SuppressWarnings({ "unchecked", "resource", "rawtypes" })
	public static ContextManager scan(Plugin plugin, String configPlugin) {
				
		System.out.println("[BetterCommands] =========== Scanning plugin '" + plugin.getName() + "'... ===========");				
				
		if (configPlugin != null) {
			if (ConfigRegistry.getConfig(configPlugin) == null) {
				throw new RuntimeException("Error while scanning plugin '" + plugin.getName() + "'. The plugin wants to use the config of plugin '" + configPlugin + "' even tho this config doesn't exist");
			} else {
				ConfigRegistry.useConfigOf(plugin, configPlugin);
			}
		}
		
		JarFile jar = null;
			
		try {		
			
			Method getJarFileMethod = JavaPlugin.class.getDeclaredMethod("getFile");
			getJarFileMethod.setAccessible(true);			
			jar = new JarFile((File) getJarFileMethod.invoke(plugin));
			
			List<String> commands = new ArrayList<>();			
			HashMap<Class<?>, Object> instances = new HashMap<>();
			
			for (Enumeration<JarEntry> entries = jar.entries(); entries.hasMoreElements(); ) {
				JarEntry entry = entries.nextElement();
				if (entry.isDirectory() || !entry.getName().endsWith(".class")) {
					continue;		
				}
																		
				className = entry.toString().replace("/", ".").substring(0, entry.toString().length() - ".class".length());
				for (int i = 1; i < 3; i++) {
					className = className.charAt(className.length() - i) == '$' ? className.substring(0, className.length() - i) : className;
				}
				
				if (excludedPackages.stream().anyMatch(packageName -> className.startsWith(packageName)) || loaded.contains(className)) {
					continue;
				}
				
				loaded.add(className);
					
				Class<?> clazz;
				
				try {				
					clazz = Class.forName(className);
				} catch (Exception e) {
					continue;
				}				
				
				if (DefaultConfig.class.isAssignableFrom(clazz)) {
					try {
						Constructor<?> constructor = clazz.getDeclaredConstructor(Plugin.class);	
						constructor.setAccessible(true);
						Object object = constructor.newInstance(plugin);					

						System.out.println("[BetterCommands] Plugin '" + plugin.getName() + "' registered config '" + clazz.getName() + "'");
						ConfigRegistry.registerConfig((BetterCommandConfig) object);
						
					} catch (Exception e) {
						e.printStackTrace();
						throw new RuntimeException("Could not instantiate config '" + className + "'. Check if the required constructor is available.");
					}	
				} else if (CommandParameter.class.isAssignableFrom(clazz)
						|| SubValidator.class.isAssignableFrom(clazz)
						|| DefaultConfig.class.isAssignableFrom(clazz)) {
					
					Class<?> forClass = null;
					
					try {
						forClass = (Class<?>) ((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments()[0];
					} catch(Exception e) {
						continue;
					}
					
					if (CommandParameter.class.isAssignableFrom(clazz) && !CommandParameter.class.equals(clazz)){
						System.out.println("[BetterCommands] Registered parameter '" + clazz.getName() + "' for class '" + forClass.getName() + "'");
						ParameterRegistry.registerParameter(forClass, (Class<? extends CommandParameter<?>>) clazz);
					} else if (SubValidator.class.isAssignableFrom(clazz) && !SubValidator.class.equals(clazz)){
						System.out.println("[BetterCommands] Registered sender validator '" + forClass.getName() + "'");
						ValidatorRegistry.registerCommandValidator(forClass, (Class<? extends SubValidator>) clazz);
					}								
				
				} else if (PermissionHook.class.isAssignableFrom(clazz) && !PermissionHook.class.equals(clazz)) {
					HookRegistry.registerHook((Class<? extends PermissionHook>) clazz);
					System.out.println("[BetterCommands] Registered permision hook '" + clazz.getSimpleName() + "' of class '" + clazz.getName() + "'");
				} else if (clazz.isAnnotationPresent(Command.class) || Stream.of(clazz.getDeclaredMethods()).anyMatch(method -> method.isAnnotationPresent(Command.class))) {
					commands.add(className);
				} else if(clazz.isAnnotationPresent(SimpleParameter.class)) {
					
					Field content = Stream.of(clazz.getDeclaredFields()).filter(field -> field.isAnnotationPresent(Content.class)).findAny().orElse(null);
									
					if (content == null) {
						throw new RuntimeException("Simple paremter class '" + clazz.getName() + "' doesn't provide a static content list");
					}
					
					if (!Modifier.isStatic(content.getModifiers())) {
						throw new RuntimeException("Content field for class '" + clazz.getName() + "' has to be static");
					}
					
					content.setAccessible(true);
					
					Method identifier = Stream.of(clazz.getDeclaredMethods()).filter(method -> method.isAnnotationPresent(Identify.class)).findAny().orElse(null);
					
					if (identifier == null) {
						throw new RuntimeException("Simple paremter class '" + clazz.getName() + "' doesn't provide a identifier function");
					}
					
					SimpleParameter parameter = clazz.getDeclaredAnnotation(SimpleParameter.class);
					
					if (!parameter.notFoundError().equals("null")) {
						ParameterRegistry.registerSimpleParameter(clazz, (List) content.get(null), e -> {
							try {
								identifier.setAccessible(true);
								return identifier.invoke(e).toString();
							} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
								return null;
							}
						}, parameter.notFoundError(), parameter.duplicateError());
					} else if (!parameter.value().equals("null")) {
						ParameterRegistry.registerSimpleParameter(clazz, parameter.value(), (List) content.get(null), e -> {
							try {
								identifier.setAccessible(true);
								return identifier.invoke(e).toString();
							} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
								return null;
							}
						});
					} else {
						ParameterRegistry.registerSimpleParameter(clazz, (List) content.get(null), e -> {
							try {
								identifier.setAccessible(true);
								return identifier.invoke(e).toString();
							} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
								return null;
							}
						});
					}					
					
					System.out.println("[BetterCommands] Registered simple parameter for class '" + clazz.getName() + "'");
					
				}
				
				
				
				if (Stream.of(clazz.getDeclaredMethods()).anyMatch(method -> method.isAnnotationPresent(Validator.class))) {
					Stream.of(clazz.getDeclaredMethods())
						.filter(method -> method.isAnnotationPresent(Validator.class))
						.map(method -> {
							method.setAccessible(true);
							return method;
						})
						.forEach(method -> {
							
							Validator validator = method.getDeclaredAnnotation(Validator.class);
							Class<?> forClass = method.getReturnType() == Object.class || method.getReturnType() == ValidationPayload.class 
									? forClass = validator.value() : method.getReturnType();
															
							if (method.getParameterTypes().length >= 2) {
								if (!CommandSender.class.isAssignableFrom(method.getParameterTypes()[0])) {
									throw new RuntimeException("The first parameter has to be assignable from CommandSender for method '" + method + "'");
								}
								if (!method.getParameterTypes()[1].equals(CommandValue.class)) {
									throw new RuntimeException("The second parameter has to be CommandValue<" + forClass.getSimpleName() + "> for method '" + method + "'");
								}
								
							} else {
								throw new RuntimeException("'" + method + "' contains to few parameters to be a validator");
							}
									
							Method tabComplete = Stream.of(clazz.getDeclaredMethods())
														.filter(m -> m.isAnnotationPresent(TabCompleter.class) 
																&& m.getDeclaredAnnotation(TabCompleter.class).value() == forClass)
														.map(m -> {
															m.setAccessible(true);
															return m;
														})
														.findAny().orElse(null);
							
							if (tabComplete != null) {
								if (!tabComplete.getReturnType().equals(List.class)) {
									throw new RuntimeException("Tab completer method '" + tabComplete + "' has to return 'List<String>'");
								}
								if (tabComplete.getParameterTypes().length == 0 || !CommandSender.class.isAssignableFrom(tabComplete.getParameterTypes()[0])) {
									throw new RuntimeException("The first parameter has to be assignable from CommandSender for method '" + method + "'");									
								}
							} 
							
							if (JavaPlugin.class.isAssignableFrom(clazz)) {
								instances.put(clazz, JavaPlugin.getProvidingPlugin(clazz));
							} else {
								instances.put(clazz, createInstance(clazz));
							}
							
							AnnotationParameter parameter = new AnnotationParameter(null, null);
							ParameterRegistry.registerParameter(forClass, (Class<? extends CommandParameter<?>>) parameter.getClass());
							AnnotationParameterMethods.setMethods(forClass, new AnnotationParameterMethods(instances.get(clazz), method, tabComplete, validator.error()));
							System.out.println("[BetterCommands] Registered parameter for class '" + forClass.getName() + "'");
									
						});
				}
			}
			
			jar.close();
					
			commands.stream().forEach(command -> {
				
				Class<?> clazz = null;
				try {
					clazz = Class.forName(command);
				} catch (ClassNotFoundException e) {
					return;
				}
				
				if (JavaPlugin.class.isAssignableFrom(clazz)) {
					CommandLoader.addAnnotations(plugin);			
					return;
				}
				
				CommandLoader.addAnnotations(instances.getOrDefault(clazz, createInstance(clazz)));
				
			});
			
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IOException e) {
			e.printStackTrace();
		}
		
		CommandLoader.load(plugin);
		
		List<BetterCommand> commands = getBetterCommands().stream().filter(command -> command.getPlugin().equals(plugin.getName())).collect(Collectors.toList());
		Bukkit.getHelpMap().addTopic(new HelpTopic() {
			
			@Override
			public boolean canSee(CommandSender sender) {
				return commands.stream().anyMatch(command -> command.testPermissionSilent(sender));
			}
			@Override
			public String getName() {
				return plugin.getName();
			}
			@Override
			public String getFullText(CommandSender forWho) {

				StringBuilder builder = new StringBuilder();
				
				builder.append(ChatColor.GRAY + "Below is a list of all " + plugin.getName() + " commands:");
				
				for (BetterCommand command : commands) {
					builder.append("\n" + ChatColor.GOLD + "/" + command.getName() + ": " + ChatColor.RESET + command.getDescription());
				}
				
				return builder.substring(0, builder.length()).toString();
				
			}
			
			@Override
			public String getShortText() {
				return "All commands for " + plugin.getName();
			}
		});
				
		return new ContextManager(plugin);
		
	}
	
	private static Object createInstance(Class<?> clazz) {
		try {
			Constructor<?> constructor = clazz.getDeclaredConstructor();	
			constructor.setAccessible(true);
			Object object = constructor.newInstance();					
			return object;
		} catch (Exception e) {
			throw new RuntimeException("Could not instantiate object with class '" + clazz.getName() + "'. Check if the class has no constructor.");
		}
	}
	
	private String command;	
	private List<SubCommand> subCommands;
	private String rootPermission;
	private String plugin;
	
	private String helpMessage;
	private String permissionDeniedMessage;
	private String syntaxErrorMessage;
	private String invalidSenderMessage;	
	private String noActionMessage;
	private String enumErrorMessage;
	
	private String prefix;
	private ChatColor normalColor;
	private ChatColor subNormalColor;
	private ChatColor successColor;
	private ChatColor failedColor;
	private ChatColor highlightColor;
	private ChatColor subHighlightColor;
	private ChatColor minorColor;	
	private List<SubValidator> validators;
	private MessageTranslator translator;
	
	private boolean showInHelp;
	private boolean showPermission;
	private boolean showOnPermission;
	private boolean appendPrefixOnError;
	private boolean requiresExactPermission;
	private boolean hasPacketInjector;
	
	private boolean isConversation;
	
	private BetterCommandConfig overrideConfig;
	
	public BetterCommand(Plugin plugin, String command, String... aliases) {
		this(plugin, command, null, aliases);
	}
	
	public BetterCommand(Plugin plugin, String command, BetterCommandConfig config, String... aliases) {

		super(command);
		
		this.plugin = plugin != null ? plugin.getName() : "";
		this.command = command;
		this.subCommands = new ArrayList<SubCommand>();
		this.rootPermission = (plugin != null ? plugin.getName().toLowerCase() + "." : "") + command.toLowerCase();
		this.setPermission(getRootPermission());
		if (aliases != null) {
			this.setAliases(Arrays.asList(aliases));
		}
		
		if (config != null) {
			this.overrideConfig = config;
		}
		
		VersionManagement.registerCommand(this);
		
		betterCommands.add(this);
		
		this.setPrefix(this.getConfig().getPrefix());
		this.setNormalColor(this.getConfig().getNormalColor());
		this.setSubNormalColor(this.getConfig().getSubNormalColor());
		this.setSuccessColor(this.getConfig().getSuccessColor());
		this.setFailedColor(this.getConfig().getFailedColor());
		this.setHighlightColor(this.getConfig().getHighlightColor());
		this.setSubHighlightColor(this.getConfig().getSubHighlightColor());
		this.setMinorColor(this.getConfig().getMinorColor());
		
		this.setAppendPrefixOnError(this.getConfig().appendPrefixOnError());
		this.setTranslator(this.getConfig().getTranslator());
		
		this.setHelpMessage(this.getConfig().getHelpMessage());		
		this.setPermissionDeniedMessage(this.getConfig().getPermissionDeniedMessage());
		this.setSyntaxErrorMessage(this.getConfig().getSyntaxErrorMessage());
		this.setInvalidSenderMessage(this.getConfig().getInvalidSenderMessage());
		this.setNoActionMessage(this.getConfig().getNoActionMessage());
		this.setEnumErrorMessage(this.getConfig().getEnumErrorMessage());
		
		this.setShowInHelp(this.getConfig().showInHelp());
		this.setShowPermission(this.getConfig().showPermision());
		this.setValidators(this.getConfig().getValidators().stream().collect(Collectors.toList()));		
		this.setShowOnPermission(this.getConfig().showOnPermission());
		this.setConversation(this.getConfig().isConversation());
		this.setRequiresExactPermission(this.getConfig().requiresExactPermission());
		this.setHasPacketInjector(this.getConfig().hasPacketInjector());		
		
		this.setDescription(this.getTranslator().translate(false, this, getHelpMessage()).toLegacyText());
		this.setUsage("/" + command);
		
	}	
	
	public BetterCommand(String command, String... aliases) {
		this(Main.getInstance(), command, aliases);		
		this.setRootPermission(command);
	}	
	
	public final String getCommand() {
		return this.command;
	}
	public final void setRootPermission(String rootPermission) {
		this.rootPermission = rootPermission;
		this.setPermission(rootPermission);
	}
	public final String getRootPermission() {
		return rootPermission;
	}
	
	public void clearPermission() {
		this.rootPermission = null;
		this.setPermission(null);
	}
		
	@Override
	public final boolean execute(CommandSender sender, String label, final String[] args) {
				
		if (!this.testPermissionSilent(sender)) {
			this.sendErrorMessage(sender, this.getPermissionDeniedMessage());
			return true;
		}
		
		String[] sortOut = args;
		
		List<SubCommand> subCommands = this.getSubCommands().size() == 1 
									? this.getSubCommands() 
									: this.getSubCommands().stream()
										.sorted((first, second) -> second.getInlineLength() - first.getInlineLength())
										.sorted((first, second) -> second.getPathArray().length - first.getPathArray().length)										
										.filter(sc -> sc.matchesPath(args)).collect(Collectors.toList());
							
		if (subCommands.isEmpty() && this.getSubCommand("help") != null) {
			subCommands.add(this.getSubCommand("help"));
		}
									
		if (subCommands.size() == 1 && subCommands.get(0).getPath().equals("help")) {
			if (args.length == 0) {
				sortOut = new String[] {"help"};
			} else if (!args[0].equals("help")) {
				String[] temp = new String[args.length + 1];
				temp[0] = "help";
				for (int i = 0; i < args.length; i++) {
					temp[i + 1] = args[i];
				}
				sortOut = temp;
			}  
			System.out.println(String.join(" ", sortOut));
		}
									
		if (subCommands.isEmpty()) {
			this.sendErrorMessage(sender, this.getNoActionMessage());
			return true;
		}
		
		ExecutionPayload payload = null;
		
		for (SubCommand subCommand : subCommands) {
			payload = subCommand.internalExecute(sender, sortOut);
			if (!payload.hasFailed()) {
				return true;
			}
		}
		
		payload.getSubCommand().sendErrorMessage(sender, payload.getErrorMessage(), payload.getVars().toArray());
		
		return true;
		
	}	
	
	private void sendErrorMessage(CommandSender sender, String message, Object... args) {
		sender.sendMessage(this.getTranslator().translateError(this, message, args).toLegacyText());
	}
	
	public final void registerSubCommand(SubCommand subCommand) {
		
//		if (this.getSubCommand(subCommand.getPath()) != null) {
//			throw new RuntimeException("Error while loading " + subCommand.toString() + ". Path does already exist");
//		}
		
		if (subCommand.hasCorrectFormedPath()) {
			this.subCommands.add(subCommand);
			subCommand.register();
		
			if (this.subCommands.size() == 1) {
				this.setUsage(this.getTranslator().translate(false, subCommand, subCommand.getUsage(true)).toLegacyText());
				this.setDescription(this.getTranslator().translate(false, subCommand, subCommand.getHelpMessage()).toLegacyText());
			} else {
				this.setUsage(this.getSubCommand("help") != null ? "/" + command + " help" : this.getTranslator().translate(false, this, ":fNo explicit usage").toLegacyText());
				this.setDescription(this.getSubCommand("help") != null ? "Type '/" + command + " help' for help" : this.getTranslator().translate(false, subCommand, this.getHelpMessage()).toLegacyText());
			}
			
		}
		
	}
	
	public final void registerHelpSubCommand(boolean withPermission) {
		
		this.registerSubCommand(new SubCommand(this, "help") {

			@Override
			public void register() {

				this.setHelpMessage("Shows a help message");
				if (!withPermission) {
					this.clearPermission();
				}
				this.addParameter("current", ParameterType.OPTIONAL_ARRAY);

			}
			
			@Override
			public void execute(CommandSender sender, ParameterSet parameterSet) {

				Stream.of(parameterSet.getParameters()).forEach(System.out::println);
				System.out.println(parameterSet.isSet("current"));
				
				if (BetterCommand.this.getSubCommands().size() == 1) {
					sender.sendMessage("E:No sub commands were found");
					return;
				} else {
					BetterCommand.this.getConfig().sendHelpMessage(sender, BetterCommand.this, parameterSet.isSet("current") ? parameterSet.get("current").asString().trim() : null);
				}
					
			}
			
		});
		
		this.setUsage("/" + command + " help");
		this.setDescription("Type '/" + command + " help' for help");
		
	}
	
	public final void registerHelpSubCommand() {		
		this.registerHelpSubCommand(true);		
	}
	
	public final SubCommand getSubCommand(String path) {		
		return this.subCommands.stream().filter(subCommand -> subCommand.getPath().equals(path)).findAny().orElse(null);
	}
	
	public final BetterCommandConfig getConfig() {		
		return overrideConfig != null ? overrideConfig : ConfigRegistry.getConfig(plugin);		
	}

	public final String getHelpMessage() {
		return helpMessage;
	}
	public final void setHelpMessage(String helpMessage) {
		this.helpMessage = helpMessage;
	}

	public final String getPermissionDeniedMessage() {
		return permissionDeniedMessage;
	}
	public final void setPermissionDeniedMessage(String permissionDeniedMessage) {
		this.permissionDeniedMessage = permissionDeniedMessage;
	}

	public final String getSyntaxErrorMessage() {
		return syntaxErrorMessage;
	}
	public final void setSyntaxErrorMessage(String syntaxErrorMessage) {
		this.syntaxErrorMessage = syntaxErrorMessage;
	}
	
	public final String getInvalidSenderMessage() {
		return invalidSenderMessage;
	}
	public final void setInvalidSenderMessage(String invalidSenderMessage) {
		this.invalidSenderMessage = invalidSenderMessage;
	}

	public final ChatColor getNormalColor() {
		return normalColor;
	}
	public final void setNormalColor(ChatColor normalColor) {
		this.normalColor = normalColor;
	}

	public final ChatColor getSuccessColor() {
		return successColor;
	}
	public final void setSuccessColor(ChatColor successColor) {
		this.successColor = successColor;
	}

	public final ChatColor getFailedColor() {
		return failedColor;
	}
	public final void setFailedColor(ChatColor failedColor) {
		this.failedColor = failedColor;
	}
	
	public final ChatColor getHighlightColor() {
		return highlightColor;
	}
	public final void setHighlightColor(ChatColor highlightColor) {
		this.highlightColor = highlightColor;
	}
	
	public final ChatColor getMinorColor() {
		return minorColor;
	}
	public final void setMinorColor(ChatColor minorColor) {
		this.minorColor = minorColor;
	}

	public final String getPrefix() {
		return prefix;
	}
	public final void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	
	public final ChatColor getSubNormalColor() {
		return subNormalColor;
	}
	public final void setSubNormalColor(ChatColor subNormalColor) {
		this.subNormalColor = subNormalColor;
	}

	public final ChatColor getSubHighlightColor() {
		return subHighlightColor;
	}
	public final void setSubHighlightColor(ChatColor subHighlightColor) {
		this.subHighlightColor = subHighlightColor;
	}
		
	public final boolean showInHelp() {
		return showInHelp;
	}
	public final void setShowInHelp(boolean showInHelp) {
		this.showInHelp = showInHelp;
	}

	public final boolean showPermision() {
		return showPermission;
	}
	public final void setShowPermission(boolean showPermission) {
		this.showPermission = showPermission;
	}
	
	@Override
	public final List<SubValidator> getValidators() {
		return this.validators;
	}
	
	public final void setValidators(List<SubValidator> validators) {
		this.validators = validators;
	}
	
	public final void clearValidators() {
		validators.clear();		
	}
	
	@Override
	public MessageTranslator getTranslator() {
		return translator;
	}
	
	public void setTranslator(MessageTranslator translator) {
		this.translator = translator;
	}
	
	@Override
	public final boolean showOnPermission() {
		return this.showOnPermission;
	}
	
	public final void setShowOnPermission(boolean showOnPermission) {
		this.showOnPermission = showOnPermission;
	}
	
	@Override
	public final boolean isConversation() {
		return isConversation;
	}
	
	public final void setConversation(boolean isConversation) {
		this.isConversation = isConversation;
	}
		
	@Override
	public final boolean appendPrefixOnError() {
		return this.appendPrefixOnError;
	}
	
	public final void setAppendPrefixOnError(boolean appendPrefixOnError) {
		this.appendPrefixOnError = appendPrefixOnError;
	}
	
	@Override
	public final boolean requiresExactPermission() {
		return this.requiresExactPermission;
	}
	
	public final void setRequiresExactPermission(boolean requiresExactPermission) {
		this.requiresExactPermission = requiresExactPermission;
	}
	
	@Override
	public final List<String> tabComplete(CommandSender sender, String alias, String[] args) {
		List<String> ret = new ArrayList<>();
		for (SubCommand command : this.getSubCommands()) {
			ret.addAll(command.getTabComplete(sender, args));
		}
		return ret.stream().distinct().filter(tab -> args.length == 0 || tab.toLowerCase().startsWith(args[args.length - 1].toLowerCase())).collect(Collectors.toList());
	}
	
	public final void addValidator(SubValidator validator) {
		validators.add(validator);
	}

	@Override
	public final Plugin getOwner() {
		return this.getConfig().getOwner();
	}

	@Override
	public boolean testPermissionSilent(CommandSender target) {
		if (this.getRootPermission() == null) {
			return true;
		}
		
		return target.hasPermission(this.getRootPermission()) 
				|| target.getEffectivePermissions().stream().anyMatch(attachment -> attachment.getPermission().startsWith(this.getRootPermission()));		
	}
		
	public final List<SubCommand> getSubCommands() {
		return subCommands;
	}	
	
	public final String getPlugin() {
		return plugin;
	}
	
	public final void setNoActionMessage(String noActionMessage) {
		this.noActionMessage = noActionMessage;
	}
	
	@Override
	public final String getNoActionMessage() {
		return noActionMessage;
	}
	
	@Override
	public boolean hasPacketInjector() {
		return hasPacketInjector;
	}
	
	public void setHasPacketInjector(boolean hasPacketInjector) {
		this.hasPacketInjector = hasPacketInjector;
	}
	
	@Override
	public String getEnumErrorMessage() {
		return this.enumErrorMessage;
	}
	
	public void setEnumErrorMessage(String enumErrorMessage) {
		this.enumErrorMessage = enumErrorMessage;
	}
	
}
