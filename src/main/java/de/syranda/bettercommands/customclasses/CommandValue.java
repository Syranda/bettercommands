package de.syranda.bettercommands.customclasses;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;

import de.syranda.bettercommands.customclasses.parameter.CommandParameter;
import de.syranda.bettercommands.customclasses.parameter.ParameterSet;
import de.syranda.bettercommands.customclasses.parameter.ValidationPayload;
import de.syranda.bettercommands.customclasses.parameter.validators.SubValidatorPayload;
import net.md_5.bungee.api.ChatColor;

public class CommandValue<N> {
		
	private Object object;
	private CommandParameter<N> parameter;
	private N cachedObject;
	
	public CommandValue(Object object, CommandParameter<N> parameter) {
		this.object = object;
		this.parameter = parameter;		
	}
	
	public CommandValue(Object object) {
		this(object, null);		
	}
	
	public Object asObject() {		
		return object;		
	}
	
	public boolean isInt() {
		try {
			asInt();
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
	public int asInt() {		
		return Integer.parseInt(object.toString());		
	}
	
	public String asString() {		
		return object.toString();		
	}
	
	public boolean isLong() {
		try {
			asLong();
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
	public long asLong() {		
		return Long.parseLong(object.toString());		
	}
	
	public boolean isBoolean() {
		try {
			return object.toString().equalsIgnoreCase("true") 
					|| object.toString().equalsIgnoreCase("false");
		} catch(Exception e) {
			return false;
		}
	}
	
	public boolean asBoolean() {		
		return Boolean.parseBoolean(object.toString());		
	}

	public boolean isMaterial() {
		try {
			asMaterial();
			return true;
		} catch(Exception e) {
			return false;
		}		
	}
	
	public Material asMaterial() {
		return Material.valueOf(object.toString());		
	}
	
	public boolean isNull() {
		return object == null || this.asString().equals("null");		
	}
	
	public ValidationPayload<N> validate(CommandSender sender, ParameterSet parameterSet) {		
		
		if (this.cachedObject != null) {
			return new ValidationPayload<N>().successWithObject(this.cachedObject);
		}
		
		if (this.getParameter().getParameterType().isOptional() && this.isNull()) {
			return new ValidationPayload<N>().successWithObject(null);
		}
		
		ValidationPayload<N> payload = this.parameter.validate(sender, this, parameterSet);
		
		if (!payload.hasFailed()) {
						
			List<SubValidatorPayload> failedSubs = this.parameter.getFailedSubValidators(sender, this, parameterSet);
			
			if (!failedSubs.isEmpty()) {	
				SubValidatorPayload subValidatorPayload = failedSubs.get(0);
				subValidatorPayload.getVars().forEach(x -> payload.addVar(x));
				return payload.failWithMessage(subValidatorPayload.getErrorMessage());
			}
			
			this.cachedObject = payload.getValidatedObject();
			return payload;
		} else {
			return payload;
		}		
	}
	
	public N asValidatedObject() {		
		return this.cachedObject;	
	}
	
	@Deprecated
	public Object asValidatedObject(CommandSender sender) {		
		return parameter != null ? parameter.validate(sender, this, null) : object;		
	}
	
	public double asDouble() {
		return Double.parseDouble(object.toString());		
	}
	
	public boolean isDouble() {
		try {
			asDouble();
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
	public String asColorString() {
		return ChatColor.translateAlternateColorCodes('&', asString());
	}
	
	public CommandParameter<?> getParameter() {
		return parameter;
	}
	
	public CommandValue<N> or(Object or) {
		return object == null ? new CommandValue<N>(or, parameter) : this;
	}	
	
	public boolean isByte() {
		try {
			asByte();
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
	public byte asByte() {
		return Byte.parseByte(object.toString());
	}
	
	public void setParameter(CommandParameter<N> parameter) {
		this.parameter = parameter;
	}

	public boolean isShort() {
		try {
			asShort();
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
	public short asShort() {
		return Short.parseShort(object.toString());
	}
	
	@Override
	public String toString() {
		return "CommandValue [value: " + this.object + " | parameter: " + parameter + "]";
	}
	
	public CommandValue<N> withParameter(CommandParameter<N> parameter) {
		this.parameter = parameter;
		return this;
	}

	public float asFloat() {
		return Float.parseFloat(object.toString());
	}

	public boolean isFloat() {
		try {
			asFloat();
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
}
