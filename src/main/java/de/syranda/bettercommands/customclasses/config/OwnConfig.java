package de.syranda.bettercommands.customclasses.config;

import org.bukkit.plugin.Plugin;

import net.md_5.bungee.api.ChatColor;

public class OwnConfig extends DefaultConfig {

	public OwnConfig(Plugin owner) {
		super(owner);
	}

	@Override
	public String getPrefix() {
		return ":sn[:h" + getPluginName() + ":sn] ";
	}
	
	@Override
	public ChatColor getNormalColor() {
		return ChatColor.DARK_AQUA;
	}

	@Override
	public ChatColor getSubHighlightColor() {
		return ChatColor.GOLD;
	}
	
	@Override
	public ChatColor getHighlightColor() {
		return ChatColor.DARK_PURPLE;
	}
	
	@Override
	public ChatColor getMinorColor() {
		return ChatColor.DARK_GRAY;
	}
	
	@Override
	public ChatColor getSubNormalColor() {
		return ChatColor.GRAY;
	}
	
}
