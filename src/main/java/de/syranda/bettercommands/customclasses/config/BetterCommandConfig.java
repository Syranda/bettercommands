package de.syranda.bettercommands.customclasses.config;

import java.util.List;

import org.apache.commons.lang.WordUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import de.syranda.bettercommands.customclasses.BetterCommand;
import de.syranda.bettercommands.customclasses.SubCommand;
import de.syranda.bettercommands.customclasses.messages.CommandConfig;
import de.syranda.bettercommands.customclasses.messages.MessageTranslator;
import de.syranda.bettercommands.customclasses.parameter.validators.SubValidator;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent.Action;

public interface BetterCommandConfig {
	
	public String getHelpMessage();
	public String getPermissionDeniedMessage();
	public String getSyntaxErrorMessage();
	public String getInvalidSenderMessage();
	public String getNoActionMessage();
	public String getEnumErrorMessage();
	
	public String getPrefix();
	public ChatColor getNormalColor();
	public ChatColor getSubNormalColor();
	public ChatColor getSuccessColor();
	public ChatColor getFailedColor();
	public ChatColor getHighlightColor();
	public ChatColor getSubHighlightColor();
	public ChatColor getMinorColor();	
	
	public boolean showInHelp();
	public boolean showOnPermission();
	public boolean showPermision();
	public boolean appendPrefixOnError();
	public boolean requiresExactPermission();
	
	public boolean hasPacketInjector();
	
	public boolean isConversation();
	
	public List<SubValidator> getValidators();
	public MessageTranslator getTranslator();	
	
	public Plugin getOwner();
	default String getPluginName() {
		return getOwner().getName();
	}
	
	default CommandConfig getCommandConfig() {
		return null;
	}
	
	default void sendHelpMessage(CommandSender sender, BetterCommand command, String current) {		
		sender.sendMessage(this.getHelpHeader() + (current != null ? " [starting with '" + current + "']" : ""));
		command.getSubCommands().stream().filter(sc -> {
			
			String[] currentArr = current.split(" ");
			String[] own = sc.getPathArray();
			
			for (int i = 0; i < Math.min(currentArr.length, own.length); i++) {
				if (!own[i].equals("%") && !own[i].startsWith(currentArr[i])) {
					return false;
				}
			}
			
			return true;
			
		}).sorted((first, second) -> first.getPathArray().length - second.getPathArray().length).forEach(sc -> sendSubHelpMessage(sender, sc));
	}
	
	default String getHelpHeader() {
		return ":nThere are the following commands:";
	}
	
	default void sendSubHelpMessage(CommandSender sender, SubCommand subCommand)  {
		
		if (!subCommand.showToSender(sender)) {
			return;
		}
		
		if (sender instanceof Player) {
			sender.sendMessage("|:sh\u279C {:sh/" + subCommand.getCommand().getName() 
					+ (subCommand.getPathArray().length > 0 || !subCommand.getParameters().isEmpty() ? " " : "") + ":n" + subCommand.getUsage(false) 
					+ ";:nClick to execute|" + (subCommand.getParameters().isEmpty() ? Action.RUN_COMMAND.toString() : Action.SUGGEST_COMMAND.toString()) + "\\:" + subCommand.getSuggestSyntax() +"}");
			sender.sendMessage("|:sn\t" + subCommand.getHelpMessage().replace("\n", "\n\t"));
			if (subCommand.showPermision()) {
				sender.sendMessage("|:m\tPermission: :sn" + subCommand.getPermission());
			}
			if (BetterCommand.getBetterCommand("pma").testPermissionSilent(sender)) {
				sender.sendMessage("|\t{:m[:nManage permission:m]|RUN_COMMAND\\:/pma options " + subCommand.getPermission() + "}");
			}
			if (!subCommand.getParameterDescriptions().isEmpty())
				for (int paraIndex = 0; paraIndex < subCommand.getParameterDescriptions().size(); paraIndex++) {									
					if (!subCommand.getParameterDescriptions().get(paraIndex).equals("")) {
						sender.sendMessage("|:n\t" + WordUtils.capitalize(subCommand.getParameters().get(paraIndex).getName()) + "  :m\u279C  :sn" + subCommand.getParameterDescriptions().get(paraIndex));	
					}
				}					
		} else {
			sender.sendMessage("|:sh/" + subCommand.getCommand().getName() 
					+ (subCommand.getPathArray().length > 0 || !subCommand.getParameters().isEmpty() ? " " : "") + ":n" + subCommand.getUsage(false)
					+ " :m- :sn" + subCommand.getHelpMessage() + (subCommand.showPermision() && subCommand.getPermission() != null ? " :m(" + subCommand.getPermission() + ")" : ""));
		}
	}
	
}
