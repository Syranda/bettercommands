package de.syranda.bettercommands.customclasses.config;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.plugin.Plugin;

public class ConfigRegistry {

	private static List<BetterCommandConfig> configs = new ArrayList<>();
	private static BetterCommandConfig defaultConfig = new DefaultConfig(null);
	
	public static void registerConfig(BetterCommandConfig config) {
		
		if(configs.stream().anyMatch(conf -> config.getOwner().getName().equals(conf.getOwner().getName()))) {
			throw new RuntimeException("Plugin '" + config.getOwner().getName() + "' tried to register a config although it appears that the plugin has already registered one");
		}
		
		configs.add(config);
		
	}
	
	public static BetterCommandConfig getConfig(Plugin plugin) {		
		return plugin != null ? getConfig(plugin.getName()) : defaultConfig;
	}
	
	public static BetterCommandConfig getConfig(String plugin) {		
		return configs.stream().filter(config -> config.getOwner().getName().equals(plugin)).findAny().orElse(defaultConfig);
	}
	
	public static List<BetterCommandConfig> getConfigs() {
		return configs;
	}
	
	public static void setDefaultConfig(BetterCommandConfig defaultConfig) {
		ConfigRegistry.defaultConfig = defaultConfig;
	}
	
	public static void useConfigOf(Plugin plugin, String configPlugin) {

		try {
			Constructor<? extends BetterCommandConfig> constructor = getConfig(configPlugin).getClass().getConstructor(Plugin.class);
			registerConfig(constructor.newInstance(plugin));
			System.out.println("[BetterCommands] Plugin '" + plugin.getName() + "' is now using the config of Plugin '" + configPlugin + "'");
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
		}
		
	}
	
}
