package de.syranda.bettercommands.customclasses.config;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.plugin.Plugin;

import de.syranda.bettercommands.customclasses.messages.CommandConfig;
import de.syranda.bettercommands.customclasses.messages.DefaultMessageTranslator;
import de.syranda.bettercommands.customclasses.messages.MessageTranslator;
import de.syranda.bettercommands.customclasses.parameter.validators.SubValidator;
import net.md_5.bungee.api.ChatColor;

public class DefaultConfig implements BetterCommandConfig {

	private Plugin owner;
	private CommandConfig commandConfig;
		
	public DefaultConfig(Plugin owner) {
		this.owner = owner;
		this.commandConfig = new CommandConfig(owner);
	}

	public String getHelpMessage() {
		return ":fNo help available";
	}

	public String getPermissionDeniedMessage() {
		return ":fYou don't have permission to execute this command";
	}

	public String getSyntaxErrorMessage() {
		return ":fUsage: $0";
	}

	public String getInvalidSenderMessage() {
		return ":fThis command is only for players";
	}

	public String getPrefix() {
		return "";
	}
	
	@Override
	public String getNoActionMessage() {
		return ":fNo sub command can be associated with your syntax. If you're seeing this message your command isn't configurated well.";
	}

	public ChatColor getNormalColor() {
		return ChatColor.WHITE;
	}

	public ChatColor getSuccessColor() {
		return ChatColor.GREEN;
	}

	public ChatColor getFailedColor() {
		return ChatColor.RED;
	}

	public ChatColor getHighlightColor() {
		return ChatColor.YELLOW;
	}
	
	public ChatColor getMinorColor() {
		return ChatColor.GRAY;
	}
	
	public ChatColor getSubNormalColor() {
		return ChatColor.WHITE;
	}

	public ChatColor getSubHighlightColor() {
		return ChatColor.GOLD;
	}

	public boolean showInHelp() {
		return true;
	}

	public boolean showPermision() {
		return false;
	}

	@Override
	public List<SubValidator> getValidators() {
		return new ArrayList<>();
	}
	
	@Override
	public MessageTranslator getTranslator() {
		return new DefaultMessageTranslator();
	}

	@Override
	public boolean showOnPermission() {
		return false;
	}

	@Override
	public boolean isConversation() {
		return false;
	}

	@Override
	public boolean appendPrefixOnError() {
		return false;
	}

	@Override
	public boolean requiresExactPermission() {
		return false;
	}

	@Override
	public Plugin getOwner() {
		return this.owner;
	}
	
	@Override
	public CommandConfig getCommandConfig() {
		return commandConfig;
	}	
	
	@Override
	public boolean hasPacketInjector() {
		return true;
	}

	@Override
	public String getEnumErrorMessage() {
		return "$0 $1 wasn't found";
	}
	
}
