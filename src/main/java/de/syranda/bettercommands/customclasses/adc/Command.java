package de.syranda.bettercommands.customclasses.adc;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import de.syranda.bettercommands.customclasses.config.DefaultConfig;

@Retention(RUNTIME)
@Target({ TYPE, METHOD })
public @interface Command {
	/**
	 * Value for the command's name
	 * e.g. {@code @Command("command")} or {@code @Command(value = "command")} results in a command {@code /command}<br><br>
	 * 
	 * <b>Applicable for:</b> Class <i>and</i> Method<br><br>
	 *  
	 * <b>Note: </b>If {@code value()} returns {@code "none"} the API will use the class' / method's simple name
	 * 
	 * @return The command's name
	 */
	String value() default "none";
	/**
	 * Value for a sub command's path<br><br>
	 *  
	 * <b>Applicable for:</b> Method<br><br>
	 * 
	 * <b>Usage: </b> {@code <first>.<second>.<third>} = {@code /<command> first second third}
	 * 
	 * @return The sub command's path
	 */
	String path() default "";
	/**
	 * Value for a command's aliases<br><br>
	 * <b>Applicable for:</b> Method
	 * 
	 * @return The command's aliases
	 */
	String[] aliases() default {};
	/**
	 * <b>Applicable for:</b> Class <i>and</i> Method
	 * 
	 * @return Whether a help command should be registered
	 */
	boolean registerHelp() default false;
	/**
	 * <b>Applicable for:</b> Class <i>and</i> Method
	 * 
	 * @return Whether the help command should have a permission set
	 */
	boolean helpPermission() default true;
	/**
	 * <b>Applicable for:</b> Class <i>and</i> Method
	 * 
	 * @return Whether the command / sub command requires the executer to have the exact permission <i>(no wildcard permission)</i>
	 */
	boolean requiresExplicitPermission() default false;
	/**
	 * <b>Applicable for:</b> Method
	 * 
	 * @return Whether the sub command should be shown in the help command
	 */
	boolean showInHelp() default true;
	/**
	 * <b>Applicable for:</b> Class <i>and</i> Method
	 * 
	 * @return Whether the sub command's permission should be shown in the help command
	 */
	boolean showPermission() default false;
	/**
	 * <b>Applicable for:</b> Class <i>and</i> Method
	 * 
	 * @return Whether the sub command should only be shown if the executer has the permission to execute it
	 */
	boolean showOnPermission() default false;
	/**
	 * <b>Applicable for:</b> Class <i>and</i> Method
	 * 
	 * @return Whether the command's prefix should be appended on error messages
	 */
	boolean appendPrefixOnError() default false;
	/**
	 * Value for the command's root permission<br>
	 * 
	 * <b>Default: </b> {@code <plugin>.<command>}<br><br>
	 * <b>Note: </b> All sub command permissions will be built based on the command's root permission<br><br>
	 * 
	 * <b>Applicable for:</b> Class <i>and</i> Method
	 * 
	 * @return The command's root permission
	 */
	String rootPermission() default "none";
	/**
	 * <b>Applicable for:</b> Class <i>and</i> Method
	 * 
	 * @return Whether the command / sub command should have <b>no</b> permission
	 */
	boolean clearPermission() default false;
	/**
	 * <b>Applicable for:</b> Class <i>and</i> Method
	 * 
	 * @return The command's / sub command's prefix
	 */
	String prefix() default "none";
	/**
	 * Value for a command's / sub command's permission<br><br>
	 * 
	 * <b>Default: </b> {@code <rootpermission>.<path>}<br><br>
	 * 
	 * <b>Applicable for:</b> Class <i>and</i> Method
	 * 
	 * @return The command's / sub command's permission
	 */
	String permission() default "none";
	/**
	 * <b>Applicable for: </b>Method
	 * 
	 * @return Whether the packet reader should be injected to <b>all</b> players
	 */
	boolean globalInjection() default false;
	/**
	 * <b>Applicable for:</b> Class <i>and</i> Method
	 * 
	 * @return Whether this {@link Command} Annotation should be loaded first
	 */
	boolean initializer() default false;
	/**
	 * <b>Applicable for:</b> Method
	 * 
	 * @return Whether this sub command inherits its master command's settings
	 */
	boolean inherit() default true;
	
	/**
	 * <b>Applicable for:</b> Class <i>and</i> Method
	 * 
	 * @return Wheter a packet reader should be injected
	 */
	boolean hasReader() default true;
	
	Class<? extends DefaultConfig> config() default DefaultConfig.class;
}