package de.syranda.bettercommands.customclasses.adc.simple;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(TYPE)
public @interface SimpleParameter {
	String value() default "null";
	String notFoundError() default "null";
	String duplicateError() default "null";
}
