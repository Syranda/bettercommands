package de.syranda.bettercommands.customclasses.adc;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.plugin.Plugin;

import de.syranda.bettercommands.customclasses.BetterCommand;
import de.syranda.bettercommands.customclasses.SubCommand;
import de.syranda.bettercommands.customclasses.config.BetterCommandConfig;
import de.syranda.bettercommands.customclasses.config.DefaultConfig;
import de.syranda.bettercommands.customclasses.parameter.validators.SubValidator;
import de.syranda.bettercommands.customclasses.parameter.validators.ValidatorRegistry;

public final class CommandLoader {

	private CommandLoader() { // Singleton
	}

	private static HashMap<Object, List<Method>> annotations = new HashMap<>();
	
	private static Consumer<Object> getClassCommandLoader(Plugin plugin) { 
		return (object) -> {
			
			Command ann = object.getClass().getDeclaredAnnotation(Command.class);
			BetterCommand command = ann.value().equals("none") ? BetterCommand.getBetterCommand(object.getClass().getSimpleName().toLowerCase()) : BetterCommand.getBetterCommand(ann.value());
		
			if (command == null) {
	
				BetterCommandConfig override = null; 
				
				if (!ann.config().equals(DefaultConfig.class)) {
					
					try {
						
						Constructor<? extends DefaultConfig> constructor = ann.config().getDeclaredConstructor(Plugin.class);
						constructor.setAccessible(true);
						override = constructor.newInstance(plugin);
						
					} catch (Exception e) {
					}
					
				}			
				
				command = new BetterCommand(plugin, !ann.value().equals("none") ? ann.value() : object.getClass().getSimpleName().toLowerCase(), override, ann.aliases());
				if (!ann.rootPermission().equals("none")) {
					command.setRootPermission(ann.rootPermission());
				}
				if (ann.registerHelp()) {
					command.registerHelpSubCommand(ann.helpPermission());
				}
				if (ann.clearPermission()) {
					command.clearPermission();
				}
				if (!ann.prefix().equals("none")) {
					command.setPrefix(ann.prefix());
				}
								
				command.setRequiresExactPermission(ann.requiresExplicitPermission());			
				command.setShowInHelp(ann.showInHelp());
				command.setShowOnPermission(ann.showOnPermission());
				command.setShowPermission(ann.showPermission());
				command.setAppendPrefixOnError(ann.appendPrefixOnError());
				command.setHasPacketInjector(ann.hasReader());
				
				for (Annotation annotation : object.getClass().getDeclaredAnnotations()) {
					SubValidator validator = ValidatorRegistry.getValidator(annotation);
					if (validator != null) {
						command.addValidator(validator);
					} else if (!annotation.annotationType().equals(Array.class)
							&& !annotation.annotationType().equals(Optional.class)
							&& !annotation.annotationType().equals(Command.class)
							&& !annotation.annotationType().equals(Help.class)) {
						try {
							throw new RuntimeException("Error loading command " + command.toString() + ". Annotation '" + annotation.annotationType().getName() + "' is neither functional nor a sub validator");
						} catch (Exception e) {
							e.printStackTrace();
							continue;
						}
					}
				}
				System.out.println("[BetterCommands] Plugin '" + plugin.getName() + "' registered command '" + (!ann.value().equals("none") ? ann.value() : object.getClass().getSimpleName().toLowerCase()) + "' of class '" + object.getClass().getName() + "'");
			}
			
		};
	
	}
	
	private static BetterCommand command; // Lambda :/
	
	private static Consumer<List<Method>> getMethodsCommandLoader(Plugin plugin, Object object) {
		
		return (list) -> {
			command = object.getClass().isAnnotationPresent(Command.class) && !object.getClass().getAnnotation(Command.class).value().equals("none") ? BetterCommand.getBetterCommand(object.getClass().getAnnotation(Command.class).value()) : BetterCommand.getBetterCommand(object.getClass().getSimpleName().toLowerCase());
		
			list.forEach(method -> {
				
				Command ann = method.getDeclaredAnnotation(Command.class);
				
				if (ann.value().equals("none") && object.getClass().isAnnotationPresent(Command.class)) {
					command = object.getClass().getAnnotation(Command.class).value().equals("none") 
							? BetterCommand.getBetterCommand(object.getClass().getSimpleName().toLowerCase()) 
							: BetterCommand.getBetterCommand(object.getClass().getAnnotation(Command.class).value());
				} else {
					command = ann.value().equals("none") 
							? BetterCommand.getBetterCommand(method.getName().toLowerCase())
							: BetterCommand.getBetterCommand(method.getAnnotation(Command.class).value());
				}
				
				
				
				if (command == null) {

					BetterCommandConfig override = null; 
					
					if (!ann.config().equals(DefaultConfig.class)) {
						
						try {
							
							Constructor<? extends DefaultConfig> constructor = ann.config().getDeclaredConstructor(Plugin.class);
							constructor.setAccessible(true);
							override = constructor.newInstance(plugin);
							
						} catch (Exception e) {
						}
						
					}	
					
					command = new BetterCommand(plugin, !ann.value().equals("none") ? ann.value() : method.getName().toLowerCase(), override, ann.aliases());
					if (!ann.rootPermission().equals("none")) {
						command.setRootPermission(ann.rootPermission());
					}
					if (ann.registerHelp()) {
						command.registerHelpSubCommand(method.getDeclaredAnnotation(Command.class).helpPermission());
					}
					if (ann.clearPermission()) {
						command.clearPermission();
					}
					if (!ann.prefix().equals("none")) {
						command.setPrefix(ann.prefix());
					}
					
					command.setRequiresExactPermission(ann.requiresExplicitPermission());			
					command.setShowInHelp(ann.showInHelp());
					command.setShowOnPermission(ann.showOnPermission());
					command.setShowPermission(ann.showPermission());
					command.setAppendPrefixOnError(ann.appendPrefixOnError());
					command.setHasPacketInjector(ann.hasReader());
					
					System.out.println("[BetterCommands] Plugin '" + plugin.getName() + "' registered command '" + (!ann.value().equals("none") ? ann.value() : method.getName()) + "' of class " + object.getClass().getName());
				}
								
//				if (command.getSubCommand(method.getDeclaredAnnotation(Command.class).path()) != null) {
//					try {
//						throw new RuntimeException("Error while loading " + command.getSubCommand(method.getDeclaredAnnotation(Command.class).path()).toString() + ". Cannot register two sub commands with the same path");
//					} catch (Exception e) {
//						e.printStackTrace();
//						return;
//					}
//				}
											
				SubCommand subCommand = new SubCommand(command, method.getDeclaredAnnotation(Command.class).path()) {
					
					@Override
					public void register() {
						try {
							registerMethod(object, method);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
				};

				command.registerSubCommand(subCommand);
				
			});
			
		};
		
	}
	
	public static void addAnnotations(Object object) {
		List<Method> methods = new ArrayList<Method>();
		Stream.of(object.getClass().getDeclaredMethods()).filter(method -> method.isAnnotationPresent(Command.class)).forEach(methods::add);
		annotations.put(object, methods);
	}
	
	public static void load(Plugin plugin) {
		annotations.keySet().stream().filter(obj -> obj.getClass().isAnnotationPresent(Command.class) && obj.getClass().getDeclaredAnnotation(Command.class).initializer()).forEach(getClassCommandLoader(plugin));
		annotations.entrySet().stream().forEach(entry -> {
			
			List<Method> methods = entry.getValue().stream().filter(method -> method.getDeclaredAnnotation(Command.class).initializer()).collect(Collectors.toList());
			getMethodsCommandLoader(plugin, entry.getKey()).accept(methods);
			
		});
		annotations.keySet().stream().filter(obj -> obj.getClass().isAnnotationPresent(Command.class) && !obj.getClass().getDeclaredAnnotation(Command.class).initializer()).forEach(getClassCommandLoader(plugin));
		annotations.entrySet().stream().forEach(entry -> {
			
			List<Method> methods = entry.getValue().stream().filter(method -> !method.getDeclaredAnnotation(Command.class).initializer()).collect(Collectors.toList());
			getMethodsCommandLoader(plugin, entry.getKey()).accept(methods);
			
		});
		annotations.clear();
	
	}
	
}
