package de.syranda.bettercommands.customclasses;

import java.util.HashMap;

import de.syranda.bettercommands.customclasses.config.BetterCommandConfig;

public final class EnumMessages {

	private EnumMessages() {
	}
	
	private static HashMap<Enum<?>, String> names = new HashMap<>();
	
	public static String getEnumMessage(Class<?> en, BetterCommandConfig config, String value) {
		return config.getTranslator().translateError(config, config.getEnumErrorMessage(), names.getOrDefault(en, en.getSimpleName()), value).toLegacyText();
	}
	
	public static void setName(Enum<?> en, String name) {
		names.put(en, name);
	}
	
}
