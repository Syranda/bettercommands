package de.syranda.bettercommands.customclasses;

import java.util.ArrayList;
import java.util.List;

public class ExecutionPayload extends Payload {

	private SubCommand subCommand;
	
	public ExecutionPayload(SubCommand subCommand) {
		this.set("vars", new ArrayList<>());
		this.subCommand = subCommand;
	}
	
	public boolean hasFailed() {
		return this.get("errorMessage") != null;
	}
	
	public ExecutionPayload failWithMessage(String message) {
		this.set("errorMessage", message);
		return this;
	}
	
	public String getErrorMessage() {
		return this.get("errorMessage").toString();
	}
	
	@SuppressWarnings("unchecked")
	public void addVar(String var) {
		((List<String>) this.get("vars")).add(var);
	}
	
	public SubCommand getSubCommand() {
		return this.subCommand;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getVars() {
		return (List<String>) this.get("vars");
	}
	
}
