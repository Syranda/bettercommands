package de.syranda.bettercommands.customclasses;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.plugin.Plugin;

import de.syranda.bettercommands.customclasses.messages.CommandConfig;

public class ContextManager {

	private static List<ContextManager> managers = new ArrayList<>();
	
	public static ContextManager getContextManager(Plugin plugin) {
		return managers.stream().filter(manager -> manager.getPlugin().equals(plugin)).findAny().orElse(null);
	}
	
	private Plugin plugin;

	public ContextManager(Plugin plugin) {
		this.plugin = plugin;
	}
	
	public Plugin getPlugin() {
		return plugin;
	}
	
	@Deprecated
	public void useConfig(String ofPlugin) {
	}
	
	public void registerMessage(String path, String message) {
		CommandConfig.get(plugin).registerDefault(path, message);
	}
	
}
