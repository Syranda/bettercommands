package de.syranda.bettercommands.customclasses;

import static org.junit.Assert.*;

import org.junit.Test;

import de.syranda.bettercommands.customclasses.parameter.ParameterType;

public class SubCommandTest {

	private SubCommand testSubCommand;
	
	@Test
	public void testPathLessTrueMatcher() {		
		this.testSubCommand = new SubCommand(null);
		assertTrue(this.testSubCommand.matchesPath(new String[] {"Should", "be", "true"}));				
	}
	
	@Test
	public void testPathTrueMatcher() {		
		this.testSubCommand = new SubCommand(null, "%.be.true");
		assertTrue(this.testSubCommand.matchesPath(new String[] {"Should", "be", "true"}));				
	}
		
	@Test
	public void testPathFalseMatcher() {		
		this.testSubCommand = new SubCommand(null, "%.be.false");
		assertFalse(this.testSubCommand.matchesPath(new String[] {"Should", "be", "fa"}));				
	}
	
	@Test
	public void testSuitableMin() {
		this.testSubCommand = new SubCommand(null, "%.be.true");
		this.testSubCommand.addParameter("test1", ParameterType.CONSTRAINT);
		this.testSubCommand.addParameter("test2", ParameterType.CONSTRAINT);
		assertTrue(this.testSubCommand.isSuitable(new String[] {"Should", "be", "true", "test"}));
	}
	
	@Test
	public void testSuitableMax() {
		this.testSubCommand = new SubCommand(null, "%.be.true");
		this.testSubCommand.addParameter("test1", ParameterType.CONSTRAINT);
		this.testSubCommand.addParameter("test2", ParameterType.OPTIONAL);
		assertTrue(this.testSubCommand.isSuitable(new String[] {"Should", "be", "true", "test"}));
	}
	
	@Test
	public void testSuitableMinFalse() {
		this.testSubCommand = new SubCommand(null, "%.be.false");
		this.testSubCommand.addParameter("test1", ParameterType.CONSTRAINT);
		assertFalse(this.testSubCommand.isSuitable(new String[] {"Should", "be", "false", "false"}));
	}
	
	@Test
	public void testSuitableMaxFalse() {
		this.testSubCommand = new SubCommand(null, "%.be.false");
		assertFalse(this.testSubCommand.isSuitable(new String[] {"Should", "be", "false", "test"}));
	}

}
